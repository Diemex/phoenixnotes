package de.diemex.phoenixnotes.desktop.domain;

import java.io.File;

import de.diemex.phoenixnotes.core.model.document.ImageLoader;
import javafx.scene.image.Image;

public class ImageLoaderImpl implements ImageLoader<Image> {
    @Override public Image load(File path) {
        return new Image(path.toURI().toString());
    }
}
