package de.diemex.phoenixnotes.desktop.domain;

import de.diemex.phoenixnotes.core.model.executor.MainThread;
import javafx.application.Platform;

public class MainThreadImpl implements MainThread {
    @Override public void post(Runnable runnable) {
        Platform.runLater(runnable);
    }
}
