package de.diemex.phoenixnotes.desktop.view;

import java.io.File;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder;
import de.diemex.phoenixnotes.core.model.document.FolderImpl;
import de.diemex.phoenixnotes.core.model.model.Node;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DocumentChooserView {
    Stage                      stage;
    TreeItem<DocumentOrFolder> rootNode;
    DocumentChooserPresenter   presenter;


    public DocumentChooserView(DocumentChooserPresenter presenter, Stage stage) {
        this.presenter = presenter;
        this.stage = stage;
        rootNode = new TreeItem<DocumentOrFolder>(new FolderImpl(new File("Folders")));
    }


    private void iterateRecursive(Node inNode, TreeItem<DocumentOrFolder> outNode) {
        if (inNode.getChildren().size() > 0)
            for (Node child : inNode.getChildren()) {
                TreeItem<DocumentOrFolder> outChildNode = new TreeItem<>(child.getValue());
                outNode.getChildren().add(outChildNode);
                if (child.getChildren().size() > 0) {
                    iterateRecursive(child, outChildNode);
                }
            }
        else
            outNode.getChildren().add(new TreeItem<>(inNode.getValue()));
    }


    public void initialize() {
        stage.setTitle("Document Chooser");
        VBox box = new VBox();
        final Scene scene = new Scene(box, 400, 300);
        scene.setFill(Color.LIGHTGRAY);

        iterateRecursive(presenter.getDocumentProvider().getRootNode(), rootNode);
        rootNode.setExpanded(true);
        TreeView<DocumentOrFolder> treeView = new TreeView<>(rootNode);
        //LISTENER
        treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<DocumentOrFolder>>() {
            @Override public void changed(ObservableValue<? extends TreeItem<DocumentOrFolder>> observable, TreeItem<DocumentOrFolder> oldValue, TreeItem<DocumentOrFolder> selected) {
//                System.out.println("Selected : " + selected.getValue());
                if (selected.getValue() instanceof Document)
                    presenter.onDocumentSelected((Document) selected.getValue());
            }
        });
        treeView.prefHeightProperty().bind(scene.heightProperty());
        treeView.prefWidthProperty().bind(scene.widthProperty());

        box.getChildren().add(treeView);
        Platform.runLater(new Runnable() {
            @Override public void run() {
                stage.setScene(scene);
                stage.show();
            }
        });
    }
}
