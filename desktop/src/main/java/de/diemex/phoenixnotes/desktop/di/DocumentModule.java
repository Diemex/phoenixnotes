package de.diemex.phoenixnotes.desktop.di;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.document.DocumentLoader;
import de.diemex.phoenixnotes.core.model.document.DocumentLoaderImpl;
import de.diemex.phoenixnotes.core.model.document.LnDocument;
import de.diemex.phoenixnotes.core.model.document.LnPageCache;
import de.diemex.phoenixnotes.core.model.document.LnPageLoader;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.model.SimpleRectF;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenterImpl;
import de.diemex.phoenixnotes.core.model.view.PageCache;
import de.diemex.phoenixnotes.core.model.view.PageLayout;
import de.diemex.phoenixnotes.core.model.view.VerticalPageLayout;
import de.diemex.phoenixnotes.core.model.view.Viewport;
import de.diemex.phoenixnotes.core.model.view.ViewportImpl;
import de.diemex.phoenixnotes.desktop.domain.ImageLoaderImpl;
import de.diemex.phoenixnotes.desktop.view.DocumentViewImpl;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class DocumentModule {
    DocumentPresenter<Image> mPresenter;
    ApplicationModule        mApplicationModule;


    public DocumentModule(ApplicationModule applicationModule) {
        this.mApplicationModule = applicationModule;
    }


    public DocumentPresenter<Image> createPresenter(Document document) {
        int width = 1600, height = 900;
        RectFPlus windowSize = new SimpleRectF(width, height);
        Stage stage = new Stage();
        PageCache<Image> pageCache = new LnPageCache<>((LnDocument) document, new LnPageLoader<>(mApplicationModule.getThreadExecutor(), (LnDocument) document, new ImageLoaderImpl()));
        DocumentViewImpl view = new DocumentViewImpl(stage);
        PageLayout pageLayout = new VerticalPageLayout(document);
        Viewport viewport = new ViewportImpl(pageLayout, windowSize);
        DocumentLoader loader = new DocumentLoaderImpl(document, mApplicationModule.getMainThread(), mApplicationModule.getThreadExecutor());
        DocumentPresenterImpl<Image> presenter = new DocumentPresenterImpl<>(view, windowSize, viewport, document, loader, pageCache);
        view.setPresenter(presenter);
        viewport.addPageRangeListener(presenter);
        return mPresenter;
    }
}
