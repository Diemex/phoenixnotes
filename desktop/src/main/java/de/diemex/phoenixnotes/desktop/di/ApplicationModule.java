package de.diemex.phoenixnotes.desktop.di;

import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.executor.MainThread;
import de.diemex.phoenixnotes.core.model.executor.ThreadExecutor;
import de.diemex.phoenixnotes.desktop.domain.MainThreadImpl;

public class ApplicationModule {
    InteractorExecutor mExecutor;
    MainThread         mMainThread;


    public ApplicationModule() {
        this.mExecutor = new ThreadExecutor();
        this.mMainThread = new MainThreadImpl();
    }


    public InteractorExecutor getThreadExecutor() {
        return mExecutor;
    }


    public MainThread getMainThread() {
        return mMainThread;
    }
}
