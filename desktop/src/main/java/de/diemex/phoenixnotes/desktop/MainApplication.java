package de.diemex.phoenixnotes.desktop;

import java.io.File;

import de.diemex.phoenixnotes.core.model.document.LnDocumentProvider;
import de.diemex.phoenixnotes.core.model.executor.ThreadExecutor;
import de.diemex.phoenixnotes.desktop.di.ApplicationModule;
import de.diemex.phoenixnotes.desktop.di.DocumentModule;
import de.diemex.phoenixnotes.desktop.domain.NavigatorImpl;
import de.diemex.phoenixnotes.desktop.view.DocumentChooserPresenter;
import de.diemex.phoenixnotes.desktop.view.DocumentChooserView;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApplication extends Application {
    DocumentChooserPresenter presenter;
    DocumentChooserView      view;
    ApplicationModule applicationModule = new ApplicationModule();


    public static void main(String[] args) {
        Application.launch(args);
    }


    @Override public void start(Stage primaryStage) throws Exception {
        presenter = new DocumentChooserPresenter(new LnDocumentProvider(new File("/home/diemex/Dropbox/Lecture notes/")), new ThreadExecutor(), new NavigatorImpl(new DocumentModule(applicationModule)));
        view = new DocumentChooserView(presenter, primaryStage);
        presenter.setView(view);
    }


    public ApplicationModule getApplicationModule() {
        return applicationModule;
    }
}
