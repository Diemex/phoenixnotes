package de.diemex.phoenixnotes.desktop.view;


import de.diemex.phoenixnotes.core.model.model.PointF;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;
import de.diemex.phoenixnotes.core.model.view.DocumentView;
import de.diemex.phoenixnotes.core.model.view.PageLayout;
import de.diemex.phoenixnotes.core.model.view.Viewport;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.Image;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

public class DocumentViewImpl implements DocumentView {
    Stage                    stage;
    DocumentPresenter<Image> presenter;
    Canvas                   canvas;
    final ScrollBar sc = new ScrollBar();


    public DocumentViewImpl(Stage stage) {
        this.stage = stage;
    }


    public void redraw() {
        Viewport viewport = presenter.getViewPort();
        if (!viewport.isLoaded()) return;
        GraphicsContext graphics = canvas.getGraphicsContext2D();
        graphics.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (int page : viewport.getDisplayedPageRange()) {
            PointF pagePosition = presenter.getViewPort().getPagePosition(page);
            graphics.setFill(Paint.valueOf(Color.TRANSPARENT.toString()));
            graphics.setStroke(Paint.valueOf(Color.BLACK.toString()));
            graphics.strokeRect(pagePosition.getX(),
                    pagePosition.getY(),
                    pagePosition.getX() + viewport.getPageSize(page).width(),
                    pagePosition.getY() + viewport.getPageSize(page).height());
            Image img = presenter.getPageRepo().getPage(page);
            if (img != null) {
                graphics.save();
                if (img.getWidth() != viewport.getPageSize(page).width()) {
                    float scale = (float) (viewport.getPageSize(page).width() / img.getWidth());
                    graphics.scale(scale, scale);//, pagePosition.getX(), pagePosition.getY());
                }
                graphics.drawImage(img, (int) pagePosition.getX(), (int) pagePosition.getY());
                graphics.restore();
            }
            graphics.fillText("" + page, pagePosition.getX(), pagePosition.getY() + presenter.getViewPort().getPageSize(page).height());
        }
    }


    public void startAnimation() {

    }


    @Override public void setPresenter(DocumentPresenter presenter) {
        this.presenter = presenter;
        show();
    }


    @Override public DocumentPresenter getPresenter() {
        return presenter;
    }


    public void show() {
        stage.setTitle("Document View");
        HBox vbox = new HBox();
        final Scene scene = new Scene(vbox, presenter.getViewPort().getViewportSize().width(), presenter.getViewPort().getViewportSize().height());
        scene.setFill(Color.LIGHTGRAY);
        //ScrollBar
        sc.setMin(0);
        sc.setOrientation(Orientation.VERTICAL);
        if (presenter.getPageLayout().isLoaded())
            //maximum pixel offset -> bottom of last page
            sc.setMax(presenter.getPageLayout().getPositionAndSize(presenter.getPageLayout().getPageCount() - 1).bottom());
        else presenter.getPageLayout().addLoadedCallback(new PageLayout.PageLayoutLoadCallback() {
            @Override public void onLoaded() {
                sc.setMax(presenter.getPageLayout().getPositionAndSize(presenter.getPageLayout().getPageCount() - 1).bottom());
            }
        });
        sc.valueProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                presenter.getViewPort().scroll(0, newValue.intValue() - oldValue.intValue());
            }
        });
        //Canvas
        canvas = new Canvas();
        canvas.widthProperty().bind(vbox.widthProperty().subtract(sc.getWidth()));
        canvas.heightProperty().bind(vbox.heightProperty().subtract(sc.getWidth()));
        canvas.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override public void handle(ScrollEvent event) {
                sc.setValue(sc.getValue() + event.getDeltaY());
                presenter.scroll((float) -event.getDeltaX(), (float) -event.getDeltaY());
            }
        });
        vbox.getChildren().addAll(canvas, sc);
        Platform.runLater(new Runnable() {
            @Override public void run() {
                stage.setScene(scene);
                stage.show();
            }
        });
    }
}
