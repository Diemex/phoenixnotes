package de.diemex.phoenixnotes.desktop.view;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.document.LnDocumentProvider;
import de.diemex.phoenixnotes.core.model.executor.Interactor;
import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.navigation.Navigator;

public class DocumentChooserPresenter {
    private InteractorExecutor  mExecutor;
    private LnDocumentProvider  mDocumentProvider;
    private String              mRootFilePath;
    private boolean             mLoading;
    private DocumentChooserView view;
    private Navigator           navigator;


    public DocumentChooserPresenter(LnDocumentProvider provider, InteractorExecutor executor, Navigator navigator) {
        this.mDocumentProvider = provider;
        this.mExecutor = executor;
        this.navigator = navigator;
        mLoading = true;
        mExecutor.run(new Interactor() {
            @Override public void run() {
                mDocumentProvider.load();
                mLoading = false;
                if (DocumentChooserPresenter.this.view != null)
                    DocumentChooserPresenter.this.view.initialize();
            }
        });
    }


    public boolean isLoading() {
        return mLoading;
    }


    public LnDocumentProvider getDocumentProvider() {
        return mDocumentProvider;
    }


    public void onDocumentSelected(Document document) {
        System.out.println("Selected " + document.getName());
        navigator.openDocument(document);
    }


    public void setView(DocumentChooserView view) {
        this.view = view;
    }
}
