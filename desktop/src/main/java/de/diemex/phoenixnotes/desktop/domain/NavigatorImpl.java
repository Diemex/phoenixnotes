package de.diemex.phoenixnotes.desktop.domain;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.navigation.Navigator;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;
import de.diemex.phoenixnotes.desktop.di.DocumentModule;
import javafx.scene.image.Image;

public class NavigatorImpl implements Navigator {

    DocumentModule documentModule;


    public NavigatorImpl(DocumentModule documentModule) {
        this.documentModule = documentModule;
    }


    @Override public void openDocument(Document document) {
        DocumentPresenter<Image> presenter = documentModule.createPresenter(document);
    }


    @Override public void showDocumentChooser() {

    }
}
