package de.diemex.phoenixnotes.app.view.component

import de.diemex.phoenixnotes.app.util.RectFPlusImpl
import de.diemex.phoenixnotes.core.model.model.PagePosition
import pl.polidea.robospock.RoboSpecification

class DrawingPortSpec extends RoboSpecification {


    def "initial simple layout - correct page positions"() {
        given:
        def viewport = setupViewport(100, 100, 100, 300)
        expect:
        viewport.getPagePosition(0) == new PagePosition(0, 0, 0)
        viewport.getPagePosition(1) == new PagePosition(1, 0, 100)
        viewport.getPagePosition(2) == new PagePosition(2, 0, 200)
    }


    def "simple zoom - correct page positioning"() {
        given:
        def viewport = setupViewport(100, 100, 400, 400)
        viewport.zoom(2.0F)
        expect:
        viewport.getPagePosition(1) == new PagePosition(1, 0, 200)
    }

    def "simple zoom - correct page sizes"() {
        given:
        def viewport = setupViewport(100, 100, 400, 400)
        viewport.zoom(2.0F)
        expect:
        viewport.getPageSize(0) == new RectFPlusImpl(200, 200)
        viewport.getPageSize(1) == new RectFPlusImpl(200, 200)
    }

    def "zoomed 2x - scroll should have correct page positions"() {
        given:
        def viewport = setupViewport(100, 100, 400, 400)
        viewport.zoom(2.0F)
        viewport.scroll(0, 100)
        expect:
        viewport.getPagePosition(0) == new PagePosition(0, 0, -100)
        viewport.getPagePosition(1) == new PagePosition(1, 0, 100)
    }

}