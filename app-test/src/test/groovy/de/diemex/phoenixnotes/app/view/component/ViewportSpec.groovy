package de.diemex.phoenixnotes.app.view.component

import de.diemex.phoenixnotes.app.util.ImmutableRectFPlusImpl
import de.diemex.phoenixnotes.app.util.RectFPlusImpl
import de.diemex.phoenixnotes.core.model.document.DummyDocument
import de.diemex.phoenixnotes.core.model.util.IntRange
import de.diemex.phoenixnotes.core.model.view.OnPageRangeChangedListener
import de.diemex.phoenixnotes.core.model.view.VerticalPageLayout
import de.diemex.phoenixnotes.core.model.view.ViewportImpl
import pl.polidea.robospock.RoboSpecification

class ViewportSpec extends RoboSpecification {

    int pageWidth = 210, pageHeight = 300

    def "initial simple layout - correct page range"() {
        given:
        def viewport = setupViewport(100, 100, 100, 300)
        expect:
        viewport.getDisplayedPageRange() == new IntRange(0, 2)
    }

    def "initial layout cut off last page"() {
        given:
        def viewport = setupViewport(210, 300, 210, 1467)
        expect:
        viewport.getDisplayedPageRange() == new IntRange(0, 4)
    }

    def "initial layout different page sizes"() {
        given:
        int[][] pageSizes = [[300, 210, 2], [210, 300, 1], [800, 400, 1], [210, 300, 101]] as int[][]
        def pages = new DummyDocument(pageSizes)
        def viewport = new ViewportImpl(new VerticalPageLayout(pages), new ImmutableRectFPlusImpl(2560, 1467))
        viewport.layout()
        expect:
        viewport.getDisplayedPageRange() == new IntRange(0, 5)
    }

    def "scrolling should load one more page"() {
        given:
        def viewport = setupViewport()
        viewport.scroll(0, 10)
        expect:
        viewport.getDisplayedPageCount() == 2
    }

    def "scrolling one page down should update page numbers"() {
        given:
        def viewport = setupViewport()
        viewport.scroll(0, pageHeight + 1)
        expect:
        viewport.getDisplayedPageRange().getStart() == 1
    }

    def "jumping multiple pages should update page numbers"() {
        given:
        def viewport = setupViewport()
        viewport.scroll(0, 10 * pageHeight + 1)
        expect:
        viewport.getDisplayedPageRange().getStart() == 10
    }

    def "Page events - should be called on initial layout"() {
        setup:
        OnPageRangeChangedListener subscriber = Mock(OnPageRangeChangedListener)
        when:
        def viewport = setupViewport(100, 100, 400, 400, subscriber)
        then:
        1 * subscriber.loadPage(0)
        1 * subscriber.loadPage(1)
        1 * subscriber.loadPage(2)
        1 * subscriber.loadPage(3)
        0 * _ //strict mocking. Don't allow other interactions
    }

    def "Page events - should be called when scrolling"() {
        setup:
        def subscriber = Mock(OnPageRangeChangedListener)
        def viewport = setupViewport(100, 100, 400, 400, subscriber)
        when:
        viewport.scroll(0, 200)
        then:
        1 * subscriber.unloadPage(0)
        1 * subscriber.unloadPage(1)
        1 * subscriber.loadPage(4)
        1 * subscriber.loadPage(5)
        0 * _ //strict mocking. Don't allow other interactions
    }

    def "can't scroll to negative y coordinates"() {
        given:
        def viewport = setupViewport()
        viewport.scroll(0, -10)
        expect:
        viewport.getPagePosition(0).getY() == 0
    }

    def "simple zoom - correct view boxes"(int w, int h, float z, int outT, int outL, int outR, int outB) {
        given:
        def viewport = setupViewport(100, 100, w, h)
        viewport.zoom(z)
        expect:
        viewport.getViewportSize() == new RectFPlusImpl(outL, outT, outR, outB)
        where:
        w   | h   | z   | outT | outL | outR | outB
        100 | 100 | 0.5 | 0    | 0    | 200  | 200
        200 | 200 | 2.0 | 0    | 0    | 100  | 100
    }

    def "simple zoom - correct page range"() {
        given:
        def viewport = setupViewport(100, 100, 400, 400)
        viewport.zoom(2.0F)
        expect:
        viewport.getDisplayedPageRange() == new IntRange(0, 1)
    }

    def "zoomed 2x - scroll should have correct page range"() {
        given:
        def viewport = setupViewport(100, 100, 400, 400)
        viewport.zoom(2.0F)
        viewport.scroll(0, 100)
        expect:
        viewport.getDisplayedPageRange() == new IntRange(0, 2)
    }

    def ViewportImpl setupViewport() {
        ViewportImpl viewport = new ViewportImpl(new VerticalPageLayout(new DummyDocument()), new ImmutableRectFPlusImpl(pageWidth, pageHeight))
        viewport.layout()
        return viewport
    }

    def ViewportImpl setupViewport(int pageWidth, int pageHeight, int viewportWidth, int viewportHeight) {
        ViewportImpl viewport = new ViewportImpl(new VerticalPageLayout(new DummyDocument(pageWidth, pageHeight)), new ImmutableRectFPlusImpl(viewportWidth, viewportHeight))
        viewport.layout()
        return viewport
    }

    def ViewportImpl setupViewport(int pageWidth, int pageHeight, int viewportWidth, int viewportHeight, OnPageRangeChangedListener listener) {
        ViewportImpl viewport = new ViewportImpl(new VerticalPageLayout(new DummyDocument(pageWidth, pageHeight)), new ImmutableRectFPlusImpl(viewportWidth, viewportHeight), listener)
        viewport.layout()
        return viewport
    }
}
