package de.diemex.phoenixnotes.app.content

import de.diemex.phoenixnotes.app.document.NameComparator
import de.diemex.phoenixnotes.core.model.document.Document
import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder
import de.diemex.phoenixnotes.core.model.document.Folder
import de.diemex.phoenixnotes.core.model.document.LnDocumentProvider
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import pl.polidea.robospock.RoboSpecification

class LnDocumentProviderSpec extends RoboSpecification {
    @Rule
    public TemporaryFolder mFolder

    /*
    Folder Structure
    Root:
      - chemistry
      - english
      - history
      - math
      - physics
      - politics
     */

    def "list all documents in a folder"() {
        setup:
        def lnDocumentProvider = new LnDocumentProvider(mFolder.getRoot())
        createNotebooks(mFolder.getRoot(), "chemistry", "english", "history", "math", "physics", "politics")
        lnDocumentProvider.load()
        expect:
        lnDocumentProvider.getDocuments().size() == 6
    }

    //------------------------------------------------------------------------------------------------------------------------

    /*
    Folder Structure
    Root:
      maths:
        semester 1:
        - complex numbers
        - functions with 2 variables
      - mechanics
      - thermodynamics
     */

    def createStudyDocuments() {
        createNotebooks(new File(mFolder.getRoot(), "maths" + File.separator + "semester 1"), "complex numbers", "functions with 2 variables")
        createNotebooks(mFolder.getRoot(), "mechanics", "thermodynamics")
    }

    def "find all notebooks in subfolders"() {
        setup:
        createStudyDocuments()
        def lnDocumentProvider = new LnDocumentProvider(mFolder.getRoot())
        lnDocumentProvider.load()
        def loadedDocs = lnDocumentProvider.getDocuments()
        Collections.sort(loadedDocs, new NameComparator())
        expect:
        loadedDocs.size() == 4
        loadedDocs.get(0).getName() == "complex numbers"
        loadedDocs.get(1).getName() == "functions with 2 variables"
        loadedDocs.get(2).getName() == "mechanics"
        loadedDocs.get(3).getName() == "thermodynamics"
    }

    //TODO this test is too complex and tests too many things at once
    def "build tree correctly"() {
        setup:
        createStudyDocuments()
        def lnDocumentProvider = new LnDocumentProvider(mFolder.getRoot())
        lnDocumentProvider.load()
        def rootNode = lnDocumentProvider.getRootNode()
        def children = rootNode.getValueList()
        def mathsFolder = rootNode.getNodes().get(0)/*maths*/.getChildren().get(0)/*semester 1*/.getValueList()
        Collections.sort(children, new NameComparator())
        Collections.sort(mathsFolder, new NameComparator())

        expect:
        children.size() == 3
        children.get(0) instanceof Folder
        ((DocumentOrFolder) children.get(0)).getName() == "maths"
        children.get(1) instanceof Document
        ((DocumentOrFolder) children.get(1)).getName() == "mechanics"
        children.get(2) instanceof Document
        ((DocumentOrFolder) children.get(2)).getName() == "thermodynamics"

        mathsFolder.size() == 2
        mathsFolder.get(0) instanceof Document
        ((DocumentOrFolder) mathsFolder.get(0)).getName() == "complex numbers"
        mathsFolder.get(1) instanceof Document
        ((DocumentOrFolder) mathsFolder.get(1)).getName() == "functions with 2 variables"
    }

//------------------------------------------------------------------------------------------------------------------------

    def createNotebooks(File mRoot, String... names) {
        for (String name in names)
            createNotebook(mRoot, name)
    }

    def createNotebook(File mRoot, String name) {
        def nbFolder = new File(mRoot, File.separator + name)
        nbFolder.mkdirs()
        new File(nbFolder, "notebook.xml").createNewFile()
    }
}