package de.diemex.phoenixnotes.app.content

import de.diemex.phoenixnotes.app.util.RectFPlusImpl
import de.diemex.phoenixnotes.core.model.document.LnDocument
import de.diemex.phoenixnotes.core.model.model.SimpleRectF
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import pl.polidea.robospock.RoboSpecification

//TODO Should asynchroniously return loaded pages
//TODO Should merge layers
class LnDocumentSpec extends RoboSpecification {
    @Rule
    public TemporaryFolder mFolder

    def "Should have correct page count"(int pages) {
        setup:
        createLayer1(pages)
        def lnDocument = new LnDocument(mFolder.getRoot())
        lnDocument.load()
        expect:
        lnDocument.getPageCount() == pages
        where:
        pages << [3, 99, 123]

    }

    def "Should save/load notebook's info"() {
        setup:
        //Save this doc
        def lnDocument = new LnDocument(mFolder.getRoot())
        lnDocument.setLayerCount(2)
        lnDocument.setPagesize(new RectFPlusImpl(100, 50))
        lnDocument.saveNotebookInfoToFile()
        //Load the saved doc
        def loadedDocument = new LnDocument(mFolder.getRoot())
        loadedDocument.load()
        expect:
        loadedDocument.getLayerCount() == 2
        loadedDocument.getPageSize(0) == new SimpleRectF(100, 50)
    }


    def createLayer1(int pages) {
        for (int page in 1..pages)
            mFolder.newFile("page${page}.png")
    }

//    def createLayer2(int pages) {
//        mFolder.newFile("page_2.png")
//        for (int page in 2..pages)
//            mFolder.newFile("page${page}_2.png")
//    }
}