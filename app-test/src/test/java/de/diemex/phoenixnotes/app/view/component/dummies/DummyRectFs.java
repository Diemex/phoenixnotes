package de.diemex.phoenixnotes.app.view.component.dummies;

import de.diemex.phoenixnotes.app.util.ImmutableRectFPlusImpl;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;

public class DummyRectFs {
    private static RectFPlus rect2560x1600 = new ImmutableRectFPlusImpl(1600, 2560);


    public static RectFPlus get2560x1600()
    {
        return rect2560x1600;
    }
}
