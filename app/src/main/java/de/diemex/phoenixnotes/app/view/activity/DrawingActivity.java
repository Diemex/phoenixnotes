package de.diemex.phoenixnotes.app.view.activity;

import android.app.Activity;
import android.os.Bundle;

import de.diemex.phoenixnotes.app.PhoenixNotesApplication;
import de.diemex.phoenixnotes.app.di.DrawingModule;
import de.diemex.phoenixnotes.app.view.component.DocumentViewImpl;

public class DrawingActivity extends Activity {
    DocumentViewImpl mDrawingView;
    //    DrawingComponent mDrawingComponent;
    DrawingModule    mDrawingModule;
    int screenWidth  = -1;
    int screenHeight = -1;

//
//    public DrawingComponent component() {
//        if (mDrawingComponent == null && screenHeight != -1 && screenWidth != -1) {
//            mDrawingComponent = DaggerDrawingComponent.builder()
//                    .applicationComponent(((PhoenixNotesApplication) getApplication()).component())
//                    .drawingModule(new DrawingModule(screenWidth, screenHeight))
//                    .activityModule(new ActivityModule(this))
//                    .documentModule(new DocumentModule("/sdcard/LectureNotes/test"))
//                    .build();
//        }
//        return mDrawingComponent;
//    }


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawingView = new DocumentViewImpl(this);
        setContentView(mDrawingView);
    }


    @Override public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        screenWidth = mDrawingView.getWidth();
        screenHeight = mDrawingView.getHeight();
        mDrawingModule = new DrawingModule(mDrawingView, ((PhoenixNotesApplication)getApplication()).getApplicationModule2());
//        component().drawingPresenter();
//        setContentView(component().drawingView());
//        mPresenter = new DrawingPresenterImpl(screenSize, this, mDrawingView);
//        mDrawingView.setPresenter(mPresenter);
    }
}
