package de.diemex.phoenixnotes.app.view.ln;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.unnamed.b.atv.view.AndroidTreeView;

import de.diemex.phoenixnotes.app.R;

public class LnChooseDocumentFragment extends Fragment {
    private static LnChooseDocumentPresenter mPresenter;
    private        FrameLayout               mFrameLayout;
    private        AndroidTreeView           mTreeView;


    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        TODO FIX
//        if (mPresenter == null)
//            mPresenter = new LnChooseDocumentPresenter("/storage/emulated/0/LectureNotes", this);
//        else
//            mPresenter.setView(this);
    }


    @Nullable @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mFrameLayout = (FrameLayout) inflater.inflate(R.layout.ln_choose_document, null);
        if (!mPresenter.isLoading())
            finishedLoading();
        return mFrameLayout;
    }


    public void finishedLoading() {
//        TODO FIX
//        mPresenter.getDocumentProvider().getRootNode().applyFunctionToChildren(new TreeNodePlus.Function() {
//            @Override public void apply(TreeNode node) {
//                //Sort folders alphabetically and apply folder icon or item icon
//                if (node.getChildren().size() > 0) {
//                    node.setViewHolder(new SelectableHeaderHolder(getActivity(), new IconTreeItem(R.string.ic_folder, node.getValue().toString())));
//                    TreeNodePlus.sortChildren(node, new TreeNodeComparators.FolderFirstAndLexically());
//                } else
//                    node.setViewHolder(new SelectableItemHolder(getActivity(), node.getValue().toString()));
//            }
//        });
//        getActivity().runOnUiThread(
//                new Runnable() {
//                    @Override public void run() {
//                        mTreeView = new AndroidTreeView(getActivity(), mPresenter.getDocumentProvider().getRootNode());
//                        mTreeView.setDefaultAnimation(true);
//                        mTreeView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
//                        View treeView = mTreeView.getView();
////                        ((LinearLayout) treeView.getParent()).removeAllViews();
//                        mFrameLayout.addView(treeView);
//                        updateIsLoadingView();
//                    }
//                }
//        );
    }


    private void updateIsLoadingView() {
        if (mPresenter.isLoading()) {
            mFrameLayout.findViewById(R.id.progress).setVisibility(View.VISIBLE);
        } else {
            mFrameLayout.findViewById(R.id.progress).setVisibility(View.GONE);
        }
    }
}
