package de.diemex.phoenixnotes.app.view.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;

import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;
import de.diemex.phoenixnotes.core.model.view.DocumentView;
import de.diemex.phoenixnotes.core.model.view.Viewport;

/**
 * I'm a DrawingView. I'm dumb.
 * I can only display stuff and let other handle TouchEvents.
 */
public class DocumentViewImpl extends View implements DocumentView {
    private DocumentPresenter<Bitmap> mPresenter;
    private AndroidTouchHandlers      mTouchHandlers;
    private Paint                     mBluePaint;
    private Paint                     mRedPaint;
    private Paint                     mBlackPaint;
    private Paint                     mBlackFontPaint;
    private int                       mDebugRectPos;
    private boolean                   mDebugForward;
    private final boolean mDebug = true;


    public DocumentViewImpl(Context context) {
        super(context);
        mBluePaint = new Paint();
        mBluePaint.setColor(Color.BLUE);
        mBluePaint.setTextSize(50);
        mBluePaint.setStyle(Paint.Style.FILL);
        mRedPaint = new Paint();
        mRedPaint.setColor(Color.RED);
        mRedPaint.setStyle(Paint.Style.STROKE);
        mRedPaint.setStrokeWidth(4);
        mBlackPaint = new Paint(mRedPaint);
        mBlackPaint.setStrokeWidth(2);
        mBlackPaint.setColor(Color.BLACK);
        mBlackFontPaint = new Paint(mBlackPaint);
        mBlackFontPaint.setTextSize(30);
        mBlackFontPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mTouchHandlers != null && mPresenter != null)
            mTouchHandlers.onTouch(event);
        return true;
    }


    @Override protected void onDraw(Canvas canvas) {
        if (mPresenter == null) return;
        //DEBUGGING INFORMATION
        Viewport viewport = mPresenter.getViewPort();
        printDebugInfo(canvas);
        if (!viewport.isLoaded()) return;
        //Single pages
//        for (int page : mPresenter.getViewPort().getDisplayedPageRange()) {
//            PointF pagePosition = mPresenter.getViewPort().getPagePosition(page);
//            canvas.drawRect(pagePosition.getX(),
//                    pagePosition.getY(),
//                    pagePosition.getX() + viewport.getPageSize(page).width(),
//                    pagePosition.getY() + viewport.getPageSize(page).height(),
//                    mBlackPaint);
//            Bitmap bmp = mPresenter.getPageRepo().getPage(page);
//            if (bmp != null) {
//                canvas.save();
//                if (bmp.getWidth() != viewport.getPageSize(page).width()) {
//                    float scale = viewport.getPageSize(page).width() / bmp.getWidth();
//                    canvas.scale(scale, scale, pagePosition.getX(), pagePosition.getY());
//                }
//                canvas.drawBitmap(bmp, (int) pagePosition.getX(), (int) pagePosition.getY(), null);
//                canvas.restore();
//            }
//            canvas.drawText("" + page, pagePosition.getX(), pagePosition.getY() + mPresenter.getViewPort().getPageSize(page).height(),
//                    mBlackFontPaint);
//        }
    }


    private void printDebugInfo(Canvas canvas) {
        if (mDebug) {
            Viewport viewport = mPresenter.getViewPort();
            canvas.drawRect(20 + mDebugRectPos * 30, 20, 20 + (mDebugRectPos + 1) * 30, 30, mBluePaint);
            canvas.drawText("Zoom: " + viewport.getZoomFactor(), 100, 100, mBluePaint);
            canvas.drawText((int) viewport.getOffsetX() + " | " + (int) viewport.getOffsetY(), 100, 150, mBluePaint);
            canvas.drawText(viewport.getDisplayedPageRange().getStart() + " - " + viewport.getDisplayedPageRange().getStop() + " / " + viewport.getDisplayedPageCount(), 100, 200, mBluePaint);
            //RectFPlus viewbox = viewport.getViewportSize();
            //canvas.drawText("[" + (int) viewbox.top() + ", " + (int) viewbox.left() + " | " + (int) viewbox.bottom() + ", " + (int) viewbox.right() + " ]", 100, 280, mBluePaint);

            if (mDebugForward) mDebugRectPos++;
            else mDebugRectPos--;
            if (mDebugRectPos > 1) {
                mDebugForward = false;
                mDebugRectPos = 1;
            } else if (mDebugRectPos < 0) {
                mDebugForward = true;
                mDebugRectPos = 0;
            }
            //Viewbox
//            canvas.drawRect(mPresenter.getViewPort().getViewportSize().left(),
//                    viewport.getViewportSize().top(),
//                    viewport.getViewportSize().right(),
//                    viewport.getViewportSize().bottom(),
//                    mRedPaint);
        }
    }


    //Called for every frame of our scrolling/zooming animation
    @Override public void computeScroll() {
        if (mPresenter == null) return;
        if (mPresenter.isAnimating()) {
            mPresenter.computeNextFrame();
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }


    @Override public void startAnimation() {
        ViewCompat.postInvalidateOnAnimation(this);
    }


    @Override public void setPresenter(DocumentPresenter presenter) {
        mPresenter = presenter;
    }


    public void setTouchHandlers(AndroidTouchHandlers touchHandlers) {
        this.mTouchHandlers = touchHandlers;
    }


    @Override public void redraw() {
        postInvalidate();
    }


    @Override public DocumentPresenter getPresenter()
    {
        return mPresenter;
    }
}
