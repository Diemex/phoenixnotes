package de.diemex.phoenixnotes.app.view.component;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.OverScroller;

import de.diemex.phoenixnotes.core.model.util.Direction;
import de.diemex.phoenixnotes.core.model.view.Animator;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;

/**
 * I'm a ScrollThing.
 * I build upon {@link android.widget.Scroller} and provide the necessary parameters for scrolling bounds.
 * My calculations are based on pixels.
 */
public class Scroller implements OnTouchListener, Animator {
    private float mX, mY;
    private Direction mScrollDirection = Direction.NONE;
    private OverScroller     mAndroidScroller;
    private DocumentPresenter mPresenter;
    private OnTouchListener   mScrollDetector;


    public Scroller(Context context) {
        this.mScrollDetector = new AndroidTouchGestureDetector(context, this);
        mAndroidScroller = new OverScroller(context, null, 1337, 1337, true);
    }


    public void setPresenter(DocumentPresenter presenter) {
        this.mPresenter = presenter;
    }


    private void calculateScrollDirection(float x, float y) {
        if (y == 0 && x > 0)
            mScrollDirection = Direction.RIGHT;
        else if (y == 0 && x < 0)
            mScrollDirection = Direction.LEFT;
        else if (y > 0 && x == 0)
            mScrollDirection = Direction.DOWN;
        else if (y > 0 && x > 0)
            mScrollDirection = Direction.DOWN_RIGHT;
        else if (y > 0 && x < 0)
            mScrollDirection = Direction.DOWN_LEFT;
        else if (y < 0 && x == 0)
            mScrollDirection = Direction.UP;
        else if (y < 0 && x < 0)
            mScrollDirection = Direction.UP_LEFT;
        else if (y < 0 && x > 0)
            mScrollDirection = Direction.UP_RIGHT;
    }


    public void fling(int velx, int vely) {
        mAndroidScroller.fling((int) mX, (int) mY, velx, vely, -Integer.MAX_VALUE, Integer.MAX_VALUE, -Integer.MAX_VALUE, Integer.MAX_VALUE, 0, 0);
        mPresenter.startAnimating();
    }


    public void onDown() {
        mAndroidScroller.forceFinished(true);
    }


    /**
     * Scroll by a given amount of pixels
     *
     * @param dx positive: right, negative: left
     * @param dy positive: down, negative: up
     */
    public void scrollByPx(float dx, float dy) {
        mX += dx;
        mY += dy;
        calculateScrollDirection(dx, dy);
        mPresenter.scroll(dx, dy);
    }


    public void scrollToPx(float x, float y) {
        float dx = x - mX, dy = y - mY;
        mX = x;
        mY = y;
        calculateScrollDirection(dx, dy);
        mPresenter.scroll(dx, dy);
    }


    @Override public void onTouch(MotionEvent event) {
        mScrollDetector.onTouch(event);
    }


    public boolean isAnimating() {
        boolean finished = mAndroidScroller.isFinished();
        return !finished;
    }


    public boolean computeAnimation() {
        boolean animating = mAndroidScroller.computeScrollOffset();
        if (animating) {
            float nextX = mAndroidScroller.getCurrX();
            float nextY = mAndroidScroller.getCurrY();
            if (nextX != mX || nextY != mY)
                scrollToPx(nextX, nextY);
        }
        return animating;
    }


    public float getX() {
        return mX;
    }


    public float getY() {
        return mY;
    }
}
