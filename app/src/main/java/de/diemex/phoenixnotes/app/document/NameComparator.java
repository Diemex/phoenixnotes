package de.diemex.phoenixnotes.app.document;


import java.util.Comparator;

import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder;

public class NameComparator implements Comparator<DocumentOrFolder> {

    @Override public int compare(DocumentOrFolder document1, DocumentOrFolder document2) {
        return document1.getName().compareTo(document2.getName());
    }
}
