package de.diemex.phoenixnotes.app.util;

import com.unnamed.b.atv.model.TreeNode;

import java.util.Comparator;

import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder;

public class TreeNodeComparators {
    public static class FolderFirstAndLexically implements Comparator<TreeNode> {

        @Override public int compare(TreeNode lhs, TreeNode rhs) {
            if (!lhs.isLeaf() && rhs.isLeaf())
                return -1;
            else if (lhs.isLeaf() && !rhs.isLeaf())
                return 1;
            else
                return ((DocumentOrFolder) lhs.getValue()).getName().compareToIgnoreCase(((DocumentOrFolder) rhs.getValue()).getName());
        }
    }
}
