//package de.diemex.phoenixnotes.app.document;
//
//import android.graphics.Bitmap;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import de.diemex.phoenixnotes.app.view.component.PageCache;
//import de.diemex.phoenixnotes.core.model.document.LnDocument;
//
//public class LnPageCache implements PageCache, OnPageLoadedListener {
//    private LnDocument   mDocument;
//    private LnPageLoader mPageLoader;
//    private Map<Integer, Bitmap> mPages = new HashMap<>();
//
//
//    public LnPageCache(LnDocument document, LnPageLoader pageLoader) {
//        this.mDocument = document;
//        this.mPageLoader = pageLoader;
//    }
//
//
//    @Override public void loadPage(int page) {
//        mPageLoader.loadPage(page, 1, this);
//    }
//
//
//    @Override public Bitmap getPage(int page) {
//        return mPages.get(page);
//    }
//
//
//    @Override public boolean isPageLoaded(int page) {
//        return mPages.containsKey(page);
//    }
//
//
//    @Override public void unloadPage(int page) {
//        mPages.remove(page);
//    }
//
//
//    @Override public void onPageLoaded(Bitmap bitmap, int page, float zoom) {
//        mPages.put(page, bitmap);
//    }
//}
