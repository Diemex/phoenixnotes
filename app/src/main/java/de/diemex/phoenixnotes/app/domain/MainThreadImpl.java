package de.diemex.phoenixnotes.app.domain;

import android.os.Handler;
import android.os.Looper;

import de.diemex.phoenixnotes.core.model.executor.MainThread;

public class MainThreadImpl implements MainThread {
    private Handler mHandler;


    public MainThreadImpl() {
        this.mHandler = new Handler(Looper.getMainLooper());
    }


    @Override public void post(Runnable runnable) {
        mHandler.post(runnable);
    }
}
