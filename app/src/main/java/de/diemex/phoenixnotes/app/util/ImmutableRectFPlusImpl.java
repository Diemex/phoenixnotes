package de.diemex.phoenixnotes.app.util;


public class ImmutableRectFPlusImpl extends RectFPlusImpl {
    public ImmutableRectFPlusImpl(float width, float height) {
        super(width, height);
    }


    @Override public void setTop(float top) {
    }


    @Override public void setLeft(float left) {
    }


    @Override public void setBottom(float bottom) {
    }


    @Override public void setRight(float right) {
    }
}
