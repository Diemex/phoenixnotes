package de.diemex.phoenixnotes.app.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 *
 */
public class AppInfo {
    private static Context mContext = null;


    public static void setContext(Context context) {
        mContext = context;
    }


    public static int getAppVersion() {
        PackageInfo pInfo = null;
        if (mContext != null) try {
            pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pInfo != null ? pInfo.versionCode : 0;
    }
}
