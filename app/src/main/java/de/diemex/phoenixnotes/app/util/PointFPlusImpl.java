package de.diemex.phoenixnotes.app.util;

import android.graphics.Point;

import de.diemex.phoenixnotes.core.model.model.PointF;

public class PointFPlusImpl extends android.graphics.PointF implements PointF {

    public PointFPlusImpl() {
        super();
    }


    public PointFPlusImpl(float x, float y) {
        super(x, y);
    }


    public PointFPlusImpl(Point p) {
        super(p);
    }


    @Override public float getX() {
        return x;
    }


    @Override public float getY() {
        return y;
    }


    @Override public void setX(float x) {
        this.x = x;
    }


    @Override public void setY(float y) {
        this.y = y;
    }
}
