package de.diemex.phoenixnotes.app.view.component;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class AndroidTouchGestureDetector extends GestureDetector.SimpleOnGestureListener implements OnTouchListener {
    GestureDetector mGestureDetector;
    Scroller mScroller;


    public AndroidTouchGestureDetector(Context context, Scroller scroller) {
        mScroller = scroller;
        mGestureDetector = new GestureDetector(context, this);
    }


    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        mScroller.fling((int) -velocityX, (int) -velocityY);
        return true;
    }


    @Override
    public boolean onDown(MotionEvent e) {
        mScroller.onDown();
        return super.onDown(e);
    }


    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        mScroller.scrollByPx(distanceX, distanceY);
        return true;
    }


    @Override
    public void onTouch(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
    }
}
