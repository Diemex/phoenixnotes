package de.diemex.phoenixnotes.app.view.component;

import android.view.MotionEvent;

public class AndroidTouchHandlers implements OnTouchListener {
    OnTouchListener[] listeners;


    /**
     * Constructor
     *
     * @param listeners Listeners to call, the order is important as they will be called in the order of the array
     */
    public AndroidTouchHandlers(OnTouchListener[] listeners) {
        this.listeners = listeners;
    }


    @Override public void onTouch(MotionEvent event) {
        for (OnTouchListener listener : listeners)
            listener.onTouch(event);
    }
}
