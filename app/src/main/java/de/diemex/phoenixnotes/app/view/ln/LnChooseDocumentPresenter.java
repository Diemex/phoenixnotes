package de.diemex.phoenixnotes.app.view.ln;

import de.diemex.phoenixnotes.core.model.document.LnDocumentProvider;
import de.diemex.phoenixnotes.core.model.executor.Interactor;
import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;

public class LnChooseDocumentPresenter {
    private InteractorExecutor       mExecutor;
    private LnDocumentProvider       mDocumentProvider;
    private String                   mRootFilePath;
    private boolean                  mLoading;
    private LnChooseDocumentFragment mFragment;


    public LnChooseDocumentPresenter(LnDocumentProvider provider, InteractorExecutor executor, LnChooseDocumentFragment fragment) {
        this.mDocumentProvider = provider;
        this.mExecutor = executor;
        this.mFragment = fragment;
        mLoading = true;
        mExecutor.run(new Interactor() {
            @Override public void run() {
                mDocumentProvider.load();
                mLoading = false;
                if (mFragment != null)
                    mFragment.finishedLoading();
            }
        });
    }


    public boolean isLoading() {
        return mLoading;
    }


    public LnDocumentProvider getDocumentProvider() {
        return mDocumentProvider;
    }


    public void setView(LnChooseDocumentFragment fragment) {
        mFragment = fragment;
    }
}
