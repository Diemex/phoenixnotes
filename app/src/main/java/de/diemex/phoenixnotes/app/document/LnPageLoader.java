//package de.diemex.phoenixnotes.app.document;
//
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//
//import de.diemex.phoenixnotes.core.model.document.LnDocument;
//import de.diemex.phoenixnotes.core.model.executor.Interactor;
//import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
//
//public class LnPageLoader {
//    private LnDocument         mDocument;
//    private InteractorExecutor mExecutor;
//
//
//    public LnPageLoader(InteractorExecutor executor, LnDocument mDocument) {
//        this.mDocument = mDocument;
//        this.mExecutor = executor;
//    }
//
//
//    public void loadPage(final int page, final float zoom, final OnPageLoadedListener callback) {
//        mExecutor.run(new Interactor() {
//            @Override public void run() {
//                Bitmap bmp = BitmapFactory.decodeFile(mDocument.getBitmapFile(page, 1).getAbsolutePath());
//                callback.onPageLoaded(bmp, page, zoom);
//            }
//        });
//    }
//}
