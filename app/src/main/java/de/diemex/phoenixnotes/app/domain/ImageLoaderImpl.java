package de.diemex.phoenixnotes.app.domain;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;

import de.diemex.phoenixnotes.core.model.document.ImageLoader;

public class ImageLoaderImpl implements ImageLoader<Bitmap> {
    @Override public Bitmap load(File path) {
        Bitmap bmp = BitmapFactory.decodeFile(path.getAbsolutePath());
        return bmp;
    }
}
