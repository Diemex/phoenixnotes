package de.diemex.phoenixnotes.app;

import android.app.Application;

import de.diemex.phoenixnotes.app.di.ApplicationModule;

public class PhoenixNotesApplication extends Application {

    //    private ApplicationComponent applicationComponent;
    ApplicationModule mApplicationModule;


    @Override public void onCreate() {
        super.onCreate();
//        applicationComponent = DaggerApplicationComponent.builder()
//                .applicationModule(new ApplicationModule(this))
//                .build();
        mApplicationModule = new ApplicationModule(this);
    }


    public ApplicationModule getApplicationModule2() {
        return mApplicationModule;
    }
//    public ApplicationComponent component() {
//        return applicationComponent;
//    }
}