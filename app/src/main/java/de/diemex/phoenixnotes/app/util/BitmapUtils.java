package de.diemex.phoenixnotes.app.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;

import java.io.InputStream;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;

public class BitmapUtils {
    /**
     * Treats white and fully transparent pixels as empty. If one single pixel has something in it it then this returns false
     *
     * @param bmp bitmap to check
     * @return if bmp is empty
     */
    public static boolean isEmpty(Bitmap bmp) {
        bmp = Bitmap.createScaledBitmap(bmp, 100, 100, false);
        int[] pixels = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        for (int i : pixels)
            if ((i & 0xFF000000) != 0)
                return false;
        return true;
    }


    public static Bitmap createWhiteImage(RectFPlus size) {
        Bitmap bmp = Bitmap.createBitmap((int) size.width(), (int) size.height(), Bitmap.Config.ARGB_8888);//convertToBitmap(context.getResources().getDrawable(R.drawable.loading), 512, 512);
        Canvas canv = new Canvas(bmp);
        Paint white = new Paint();
        white.setColor(Color.WHITE);
        canv.drawRect(0, canv.getHeight(), 0, canv.getWidth(), white);
        return bmp;
    }


    public static RectFPlus getImageSize(InputStream inputStream) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, bitmapOptions);
        RectFPlus pageSize = new RectFPlusImpl(bitmapOptions.outWidth, bitmapOptions.outHeight);
        return pageSize;
    }


    public static Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
        Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(mutableBitmap);
        drawable.setBounds(0, 0, widthPixels, heightPixels);
        drawable.draw(canvas);

        return mutableBitmap;
    }


    /**
     * Merge multiple images with transparency into one image
     *
     * @param bmps images to merge, first image determines the canvas size
     * @return merged image or null if the input images are all null
     */
    public static Bitmap merge(Bitmap... bmps) {
        if (bmps.length == 0)
            return null;

        int first = 0;
        for (; first < bmps.length; first++)
            if (bmps[first] != null)
                break;
        if (bmps[first] == null)
            return null;
        Bitmap merged = Bitmap.createBitmap(bmps[first].getWidth(), bmps[first].getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(merged);
        boolean drawn = false;
        for (Bitmap bmp : bmps)
            if (bmp != null) {
                canvas.drawBitmap(bmp, 0, 0, null);
                drawn = true;
            }

        if (drawn)
            return merged;
        else {
            merged.recycle();
            return null; //no pixels have been set, so we would just waste memory
        }
    }
}
