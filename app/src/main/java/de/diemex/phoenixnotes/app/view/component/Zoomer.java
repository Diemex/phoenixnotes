package de.diemex.phoenixnotes.app.view.component;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import de.diemex.phoenixnotes.core.model.view.OnPositionChangedListener;

/**
 * I'm a Zoomer.
 * I know how to zoom in at a focal point.
 */
public class Zoomer implements OnTouchListener {
    private float   mZoom          = 1;
    private float   mMinZoom       = 0;
    private float   mMaxZoom       = 0;
    private boolean mMaxMinEnabled = false;
    private AndroidScaleDetector      mZoomDetector;
    private OnPositionChangedListener mListener;


    public Zoomer(Context context, OnPositionChangedListener listener) {
        this(context, 0, 0, false, listener);
    }


    public Zoomer(Context context, float mMinZoom, float mMaxZoom, OnPositionChangedListener listener) {
        this(context, mMinZoom, mMaxZoom, true, listener);
    }


    public Zoomer(Context context, float mMinZoom, float mMaxZoom, boolean mMaxMinEnabled) {
        this(context, mMinZoom, mMaxZoom, true, null);
    }


    public Zoomer(Context context, float mMinZoom, float mMaxZoom, boolean mMaxMinEnabled, OnPositionChangedListener listener) {
        this.mMinZoom = mMinZoom;
        this.mMaxZoom = mMaxZoom;
        this.mMaxMinEnabled = mMaxMinEnabled;
        this.mListener = listener;
        mZoomDetector = new AndroidScaleDetector(context, this);
    }

    public void setPositionListener(OnPositionChangedListener positionListener){
        this.mListener = positionListener;
    }


    public float getZoomFactor() {
        return mZoom;
    }


    private float ensureMinMax(float factor) {
        mZoom *= factor;
        if (mMaxMinEnabled) {
            if (mZoom > mMaxZoom) {
                factor *= mMaxZoom / mZoom;
                mZoom = mMaxZoom;
            } else if (mZoom < mMinZoom) {
                factor *= mMinZoom / mZoom;
                mZoom = mMinZoom;
            }
        }
        return factor;
    }


    public void zoomInFocalPoint(float focusX, float focusY, float factor) {
        factor = ensureMinMax(factor);
        mListener.zoom(factor);
        //TODO at focal point
    }


    @Override public void onTouch(MotionEvent event) {
        mZoomDetector.onTouch(event);
    }


    public static class AndroidScaleDetector extends ScaleGestureDetector.SimpleOnScaleGestureListener implements OnTouchListener {
        private Zoomer               mZoomer;
        private ScaleGestureDetector mScaleDetector;


        public AndroidScaleDetector(Context context, Zoomer zoomer) {
            this.mZoomer = zoomer;
            this.mScaleDetector = new ScaleGestureDetector(context, this);
        }


        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mZoomer.zoomInFocalPoint(detector.getFocusX(), detector.getFocusY(), detector.getScaleFactor());
            return true;
        }


        public void onTouch(MotionEvent event) {
            mScaleDetector.onTouchEvent(event);
        }
    }
}
