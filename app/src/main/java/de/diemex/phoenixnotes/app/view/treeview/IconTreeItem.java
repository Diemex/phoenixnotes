package de.diemex.phoenixnotes.app.view.treeview;

public class IconTreeItem {
    public int    icon;
    public String text;


    public IconTreeItem(int icon, String text) {
        this.icon = icon;
        this.text = text;
    }
}
