package de.diemex.phoenixnotes.app.di;

import android.app.Application;

import de.diemex.phoenixnotes.app.domain.MainThreadImpl;
import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.executor.MainThread;
import de.diemex.phoenixnotes.core.model.executor.ThreadExecutor;


public class ApplicationModule {

    Application        mApplication;
    InteractorExecutor mExecutor;
    MainThread         mMainThread;


    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
        this.mExecutor = new ThreadExecutor();
        this.mMainThread = new MainThreadImpl();
    }


    public Application getApplicationContext() {
        return mApplication;
    }


    public InteractorExecutor getThreadExecutor() {
        return mExecutor;
    }


    public MainThread getMainThread() {
        return mMainThread;
    }
}
