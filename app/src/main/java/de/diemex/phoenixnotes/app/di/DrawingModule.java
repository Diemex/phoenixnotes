package de.diemex.phoenixnotes.app.di;


import android.graphics.Bitmap;

import de.diemex.phoenixnotes.app.domain.ImageLoaderImpl;
import de.diemex.phoenixnotes.app.util.RectFPlusImpl;
import de.diemex.phoenixnotes.app.view.component.AndroidTouchHandlers;
import de.diemex.phoenixnotes.app.view.component.DocumentViewImpl;
import de.diemex.phoenixnotes.app.view.component.OnTouchListener;
import de.diemex.phoenixnotes.app.view.component.Scroller;
import de.diemex.phoenixnotes.app.view.component.Zoomer;
import de.diemex.phoenixnotes.core.model.document.DocumentLoader;
import de.diemex.phoenixnotes.core.model.document.DocumentLoaderImpl;
import de.diemex.phoenixnotes.core.model.document.LnDocument;
import de.diemex.phoenixnotes.core.model.document.LnPageCache;
import de.diemex.phoenixnotes.core.model.document.LnPageLoader;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenter;
import de.diemex.phoenixnotes.core.model.view.DocumentPresenterImpl;
import de.diemex.phoenixnotes.core.model.view.PageCache;
import de.diemex.phoenixnotes.core.model.view.PageLayout;
import de.diemex.phoenixnotes.core.model.view.VerticalPageLayout;
import de.diemex.phoenixnotes.core.model.view.Viewport;
import de.diemex.phoenixnotes.core.model.view.ViewportImpl;

public class DrawingModule {
    RectFPlus        mScreenSize;
    DocumentViewImpl          mDrawingView;
    DocumentPresenter<Bitmap> mPresenter;
    ApplicationModule         mApplicationModule;


    public DrawingModule(DocumentViewImpl drawingView, ApplicationModule applicationModule) {
        this.mDrawingView = drawingView;
        this.mScreenSize = new RectFPlusImpl(mDrawingView.getWidth(), mDrawingView.getHeight());
        this.mApplicationModule = applicationModule;
        createPresenter();
    }


    public DocumentPresenter<Bitmap> getPresenter() {
        if (mPresenter == null) createPresenter();
        return mPresenter;
    }


    private void createPresenter() {
        LnDocument lnDoc = new LnDocument("/sdcard/LectureNotes/test");
        DocumentLoader loader = new DocumentLoaderImpl(lnDoc, mApplicationModule.getMainThread(), mApplicationModule.getThreadExecutor());
        PageLayout pageLayout = new VerticalPageLayout(lnDoc);
        Viewport viewport = new ViewportImpl(pageLayout, mScreenSize);
        Scroller scroller = new Scroller(mDrawingView.getContext());
        Zoomer zoomer = new Zoomer(mDrawingView.getContext(), 0.3F, 3F, true);
        PageCache<Bitmap> pageCache = new LnPageCache<>(lnDoc, new LnPageLoader<>(mApplicationModule.getThreadExecutor(), lnDoc, new ImageLoaderImpl()));
        mPresenter = new DocumentPresenterImpl<>(mDrawingView, mScreenSize, viewport, lnDoc, loader, pageCache);
        AndroidTouchHandlers touchHandlers = new AndroidTouchHandlers(new OnTouchListener[]{scroller, zoomer});
        scroller.setPresenter(mPresenter);
        zoomer.setPositionListener(mPresenter);
        mPresenter.registerAnimator(scroller);
        mDrawingView.setTouchHandlers(touchHandlers);
    }
}
