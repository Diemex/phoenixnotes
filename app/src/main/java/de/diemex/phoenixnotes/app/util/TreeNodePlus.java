package de.diemex.phoenixnotes.app.util;

import com.unnamed.b.atv.model.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder;

public class TreeNodePlus extends TreeNode {
    DocumentOrFolder mValue;


    public TreeNodePlus(DocumentOrFolder value) {
        super(value);
        mValue = value;
    }


    public void setValue(DocumentOrFolder value) {
        mValue = value;
    }


    /**
     * Get all nodes that have children
     */
    public List<TreeNode> getNodes() {
        List<TreeNode> nodes = new ArrayList<>();
        for (TreeNode node : getChildren())
            if (!node.isLeaf())
                nodes.add(node);
        return nodes;
    }


    public void applyFunctionToChildren(Function func) {
        applyFunctionToChildren(this, func);
    }


    private void applyFunctionToChildren(TreeNode parent, Function func) {
        func.apply(parent);
        for (TreeNode child : parent.getChildren()) {
            if (child.getChildren().size() > 0)
                applyFunctionToChildren(child, func);
            else
                func.apply(child);
        }
    }


    @Override public DocumentOrFolder getValue() {
        return mValue;
    }


    @Override public String toString() {
        return getId() + " " + mValue.getName();
    }


    public static void sortChildren(TreeNode node, Comparator<TreeNode> comparator) {
        List<TreeNode> children = new ArrayList<>(node.getChildren());
        Collections.sort(children, comparator);
        for (TreeNode child : children)
            node.deleteChild(child);
        node.addChildren(children);
    }


    public static List<Object> getValuesList(List<TreeNode> nodes) {
        List<Object> values = new ArrayList<>();
        for (TreeNode node : nodes)
            values.add(node.getValue());
        return values;
    }


    public static interface Function {
        void apply(TreeNode node);
    }
}
