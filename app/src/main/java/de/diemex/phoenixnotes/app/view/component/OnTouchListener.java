package de.diemex.phoenixnotes.app.view.component;

import android.view.MotionEvent;

public interface OnTouchListener {
    void onTouch(MotionEvent event);
}
