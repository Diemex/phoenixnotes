package de.diemex.phoenixnotes.app.util;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;

public class RectFPlusImpl extends android.graphics.RectF implements RectFPlus {
    public RectFPlusImpl() {
        super();
    }


    public RectFPlusImpl(RectFPlus copy) {
        this.left = copy.left();
        this.top = copy.top();
        this.right = copy.right();
        this.bottom = copy.bottom();
    }


    public RectFPlusImpl(float left, float top, float right, float bottom) {
        super(left, top, right, bottom);
    }


    public RectFPlusImpl(float width, float height) {
        super(0, 0, width, height);
    }


    @Override public float top() {
        return top;
    }


    @Override public float left() {
        return left;
    }


    @Override public float bottom() {
        return bottom;
    }


    @Override public float right() {
        return right;
    }


    @Override public void setTop(float top) {
        this.top = top;
    }


    @Override public void setLeft(float left) {
        this.left = left;
    }


    @Override public void setBottom(float bottom) {
        this.bottom = bottom;
    }


    @Override public void setRight(float right) {
        this.right = right;
    }


    @Override public boolean equals(Object o) {
        if (!(o instanceof RectFPlus)) return false;
        RectFPlus rect = (RectFPlus) o;
        return left == rect.left() && top == rect.top() && right == rect.right() && bottom == rect.bottom();
    }
}
