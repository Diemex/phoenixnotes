package de.diemex.phoenixnotes.core.model.navigation;

import de.diemex.phoenixnotes.core.model.document.Document;

public interface Navigator {
    void openDocument(Document document);
    void showDocumentChooser();
}
