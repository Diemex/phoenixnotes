package de.diemex.phoenixnotes.core.model.view;

import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.model.SimpleRectF;
import de.diemex.phoenixnotes.core.model.util.IntRange;

/**
 * I'm a Viewport.
 * I know what to render and where.
 * I provide a local view coordinate system.
 */
//TODO doesnt handle rotation yet
public class ViewportImpl implements Viewport {
    private RectFPlus mViewBox, mInitialViewBox;
    private PageLayout mPageLayout;
    //    private List<RectFPlus>                  mPagesizes     = new ArrayList<>();
//    private List<PointF>                     mPagePositions = new ArrayList<>();
    private float mZoomFactor = 1;
    private IntRange                         mPageRange     = new IntRange(0, 0);
    private IntRange                         mPreviousRange = new IntRange(-1, -1);
    private List<OnPageRangeChangedListener> listeners      = new ArrayList<>();
    private boolean                          isInitialised  = false;
    private ZoomStatus                       zoomStatus     = ZoomStatus.FIT_WIDTH;


    public ViewportImpl(PageLayout mPageLayout, RectFPlus mViewPortSize) {
        this(mPageLayout, mViewPortSize, null);
    }


    public ViewportImpl(PageLayout mPageLayout, RectFPlus mViewPort, OnPageRangeChangedListener listener) {
        this.mPageLayout = mPageLayout;
        this.mViewBox = new SimpleRectF(mViewPort);
        this.mInitialViewBox = new SimpleRectF(mViewPort);
        if (listener != null)
            this.listeners.add(listener);
        if (mPageLayout.isLoaded())
            isInitialised = true;
//            initialize();
        else
            mPageLayout.addLoadedCallback(new PageLayout.PageLayoutLoadCallback() {
                @Override public void onLoaded() {
//                    initialize();
                    isInitialised = true;
                }
            });
    }


    @Override public void addPageRangeListener(OnPageRangeChangedListener listener) {
        Validate.notNull(listener);
        this.listeners.add(listener);
    }


//    /**
//     * Call this method when the PageLayout has loaded. If Layout has already loaded when
//     * the constructor executes then this method doesn't have to be called again.
//     */
//    @Override public void initialize() {
//        mPagePositions.clear();
//        mPagesizes.clear();
//        for (int page : zeroTo(mPageLayout.getPageCount())) {
//            mPagePositions.add(new SimplePointF(-1, -1));
//            RectFPlus pageSize = mPageLayout.getPositionAndSize(page);
//            mPagesizes.add(new SimpleRectF(pageSize.width(), pageSize.height()));
//        }
//        isInitialised = true;
//        layout();
//    }


    @Override public void layout() {
        if (!isInitialised) return;
//        //TODO WARNING VERTICAL LAYOUT SPECIFIC CODE
//
//        for (int page : mPageRange) {
//            RectFPlus pageLayout = getPositionAndSize(page);
//            PointF ourLayout = mPagePositions.get(page);
//            ourLayout.setX(pageLayout.left() + mX);
//            ourLayout.setY(pageLayout.top() + mY);
//        }
//        //TODO END WARNING-
//        computeZoom();
        mPageRange = mPageLayout.getPagesInRect(mViewBox);
        firePageLoadEvents();
        mPreviousRange.copyValuesFrom(mPageRange);
    }


    private void firePageLoadEvents() {
        //Fire page load/unload events for new pages
        if (!listeners.isEmpty()) {
            //loading
            for (OnPageRangeChangedListener listener : listeners) {
                for (int page : mPageRange)
                    if (!mPreviousRange.contains(page))
                        listener.loadPage(page);
                //unloading
                for (int page : mPreviousRange)
                    if (!mPageRange.contains(page) && page != -1)
                        listener.unloadPage(page);
            }
        }
    }


//    private void computeZoom() {
//        //Update page sizes with new zoom factor
//        for (int page : mPageRange) {
//            RectFPlus baseSize = mPageLayout.getPositionAndSize(page);
//            RectFPlus zoomedSize = mPagesizes.get(page);
//            zoomedSize.setRight(baseSize.width() * mZoomFactor);
//            zoomedSize.setBottom(baseSize.height() * mZoomFactor);
//        }
//        //Update page positions for zoom factor
//        for (int page : mPageRange) {
//            PointF position = mPagePositions.get(page);
//            position.setX(position.getX() * mZoomFactor);
//            position.setY(position.getY() * mZoomFactor);
//        }
//    }


//    private void ensureScreenBoundaries(float dx, float dy) {
//        //Can't scroll to negative y coords
//        if (mViewBox.top() < 0) {
//            mViewBox.offset(0, -mViewBox.top());
//            mY = 0;
//        }
//        //Can't scroll past last page
//        if (mPageRange.getStop() + 1 >= getPageLayout().getPageCount()
//                && getPagePosition(mPageRange.getStop()).getY() + mPagesizes.get(mPageRange.getStop()).height() < mInitialViewBox.bottom())
//        {
//            float offsetY = mPagesizes.get(mPageRange.getStop()).height() + getPagePosition(mPageRange.getStop()).getY() - mInitialViewBox.bottom();
//            mViewBox.offset(0, offsetY);
//            mY -= offsetY;
//        }
//        //Fit_Page no horizontal zoom
//        //Zoomed_In don't go beyond left/right border
//        //Zoomed_Out center page
//        //can't go beyond left border
//        switch (zoomStatus) {
//            case FIT_WIDTH:
//                break;
//            case ZOOMED_IN:
//                if (mViewBox.left() < 0) {
//                    mViewBox.offset(-mViewBox.left(), 0);
//                    mX = 0;
//                }
//                //can't go beyond right border
//                float maxWidth = 0;
//                for (int page : mPageRange) {
//                    if (getPagePosition(page).getX() + getPageSize(page).width() > maxWidth)
//                        maxWidth = getPagePosition(page).getX() + getPageSize(page).width();
//                }
//                if (maxWidth > mInitialViewBox.right()) {
//                    float offsetX = maxWidth - mInitialViewBox.right();
//                    mViewBox.offset(offsetX, 0);
//                    mX -= offsetX;
//                }
//                break;
//            case ZOOMED_OUT:
//                //center pages, no horizontal scroll
//                for (int page : mPageRange)
//                    getPagePosition(page).setX((mInitialViewBox.width() - getPageSize(page).width()) / 2);
//                break;
//        }
//    }


//    @Override public PointF getPagePosition(int page) {
//        return mPagePositions.get(page);
//    }


    @Override public void scroll(float dx, float dy) {
        if (!isInitialised) return;
        dx /= mZoomFactor;
        dy /= mZoomFactor;
        mViewBox.offset(dx, dy);
//        ensureScreenBoundaries(dx, dy);
        layout();
    }


    @Override public void scrollPages(float pages) {
        //TODO vertical page layout specific code
//        scroll(0, -getPageSize(mPageRange.getStart()).height() * pages);
    }


    //Zoom in at the left/top corner of the viewbox
    @Override public void zoom(float factor) {
        if (!isInitialised) return;
        mZoomFactor *= factor;
        zoomStatus = mZoomFactor == 1 ? ZoomStatus.FIT_WIDTH : mZoomFactor > 1 ? ZoomStatus.ZOOMED_IN : ZoomStatus.ZOOMED_OUT;
        mViewBox.setRight(mViewBox.left() + mInitialViewBox.width() / mZoomFactor);
        mViewBox.setBottom(mViewBox.top() + mInitialViewBox.height() / mZoomFactor);
        layout();
    }


    @Override public PageLayout getPageLayout() {
        return mPageLayout;
    }


    @Override public IntRange getDisplayedPageRange() {
        return mPageRange;
    }


    @Override public int getDisplayedPageCount() {
        //Range [1,3]: 3 - 1 = 2 -> +1 = 3 pages
        return 1 + mPageRange.getStop() - mPageRange.getStart();
    }


    @Override public RectFPlus getViewportSize() {
        return mViewBox;
    }


    @Override public float getZoomFactor() {
        return mZoomFactor;
    }


//    @Override public boolean isLoaded() {
//        return isInitialised;
//    }


    @Override public String toString() {
        return "Viewport Area: " + mViewBox.toString() + " Zoom: " + mZoomFactor + " Pages: " + mPageRange.toString();
    }
}
