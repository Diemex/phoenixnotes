package de.diemex.phoenixnotes.core.model.util;

import java.util.Iterator;
import java.util.NoSuchElementException;


public class IntRange implements Iterable<Integer>, Iterator<Integer> {

    int start, stop, step;
    int next;
    boolean iterationEnabled = true;


    /**
     * Constructor to be used with for loops. Stop will be decremented by one.
     * So IntRange(100) will loop from 0-99 -> 100 Elements
     *
     * @param stop
     */
    public IntRange(final Integer stop) {
        this(0, stop - 1, 1);
    }


    /**
     * Construct a new IntRange
     *
     * @param start start inclusive
     * @param stop  stop inclusive. Start 0, Stop 3 -> 0,1,2,3
     */
    public IntRange(final Integer start, final Integer stop) {
        this(start, stop, 1);
    }


    /**
     * Construct a new IntRange
     *
     * @param start start inclusive
     * @param stop  stop inclusive. Start 0, Stop 3 -> 0,1,2,3
     */
    public IntRange(final Integer start, final Integer stop, final boolean iterationEnabled) {
        this(start, stop, 1);
        this.iterationEnabled = iterationEnabled;
    }


    public IntRange(final Integer start, final Integer stop, final Integer step) {
        if (step.equals(0)) {
            throw new IllegalArgumentException();
        }
        this.start = start;
        this.stop = stop;
        this.step = step;
        this.next = start;
    }


    public boolean contains(int val) {
        return val >= start && val <= stop && (val - start) % step == 0;
    }


    public int getStart() {
        return start;
    }


    public void setStart(int start) {
        this.start = start;
    }


    public void set(int start, int stop) {
        this.start = start;
        this.stop = stop;
    }


    public int getStop() {
        return stop;
    }


    public void setStop(int stop) {
        this.stop = stop;
    }


    public int getStep() {
        return step;
    }


    public void setStep(int step) {
        this.step = step;
    }


    public void enableIteration(boolean allowIteration) {
        this.iterationEnabled = allowIteration;
    }


    public static IntRange oneTo(int stop) {
        return new IntRange(1, stop);
    }


    public static IntRange zeroTo(int stop) {
        return new IntRange(stop);
    }


    @Override
    public boolean hasNext() {
        return next <= stop && iterationEnabled;
    }


    @Override
    public Integer next() {
        if (next > stop) {
            throw new NoSuchElementException();
        }
        Integer lastValue = next;
        next += step;
        return lastValue;
    }


    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }


    @Override
    public Iterator<Integer> iterator() {
        next = start; // reset if previous iterator didn't iterate completely
        return this;
    }


    @Override public String toString() {
        return "[" + start + ", " + stop + " : " + step + "]";
    }


    @Override public boolean equals(Object o) {
        if (!(o instanceof IntRange)) return false;
        IntRange rng = (IntRange) o;
        return rng.start == start && rng.stop == stop && rng.step == step;
    }


    public void copyValuesFrom(IntRange copy) {
        step = copy.step;
        start = copy.start;
        stop = copy.stop;
    }
}
