package de.diemex.phoenixnotes.core.model.document;


import java.io.File;


/**
 * I'm a Document,
 * I remember all information for others.
 */
public class DocumentImpl {
    private   String  mName;
    private   String  mPdfPath;
    private   File    mFolder;
    protected boolean mOpened;
    private   int     mPageCount, mPage, mX, mY;
    private float mZoom = 1.0F;
    private DocumentParser mParser;


    public DocumentImpl(String pdfPath, File documentFolder) {
        this.mPdfPath = pdfPath;
        this.mFolder = documentFolder;
        this.mParser = new XmlDocumentParser(this, new File(documentFolder, "document.xml"));
    }


    public DocumentImpl(String pdfPath, File documentFolder, DocumentParser documentParser) {
        this.mPdfPath = pdfPath;
        this.mFolder = documentFolder;
        this.mParser = documentParser;
    }


    public boolean hasUnsavedChanges() {
        return false;
    }


    public void open() {
        mParser.read();
        mOpened = true;
    }


    public boolean isOpened() {
        return mOpened;
    }


    public void close() {
        mParser.write();
        mOpened = false;
    }


    public File getDocumentFolder() {
        return mFolder;
    }

//    ____    ______        _   _
//   / ___|  / / ___|   ___| |_| |_ ___ _ __ ___
//   \___ \ / / |  _   / _ \ __| __/ _ \ '__/ __|
//    ___) / /| |_| | |  __/ |_| ||  __/ |  \__ \
//   |____/_/  \____|  \___|\__|\__\___|_|  |___/
//


    public void setName(String name) {
        this.mName = name;
    }


    public String getName() {
        return mName;
    }


    protected void setPageCount(int pageCount) {
        mPageCount = pageCount;
    }


    public int getPageCount() {
        return mPageCount;
    }


    public int getYOffset() {
        return mY;
    }


    public void setYOffset(int y) {
        this.mY = y;
    }


    public int getXOffset() {
        return mX;
    }


    public void setXOffset(int x) {
        this.mX = x;
    }


    public int getCurrentPage() {
        return mPage;
    }


    public void setCurrentPage(int page) {
        this.mPage = page;
    }


    public float getZoom() {
        return mZoom;
    }


    public void setZoom(float zoom) {
        this.mZoom = zoom;
    }
}