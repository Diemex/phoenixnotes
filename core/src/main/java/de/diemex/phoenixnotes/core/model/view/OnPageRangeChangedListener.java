package de.diemex.phoenixnotes.core.model.view;

public interface OnPageRangeChangedListener {
    void unloadPage(int page);

    void loadPage(int page);
}
