package de.diemex.phoenixnotes.core.model.document;

import de.diemex.phoenixnotes.core.model.executor.Interactor;

public interface DocumentLoader extends Interactor {
    void execute(Document.DocumentLoadCallback callback);
}
