package de.diemex.phoenixnotes.core.model.executor;

public interface InteractorExecutor {
    void run(Interactor interactor);
}
