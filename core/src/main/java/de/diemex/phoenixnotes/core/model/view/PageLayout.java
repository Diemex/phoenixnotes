package de.diemex.phoenixnotes.core.model.view;

import java.util.Iterator;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.util.IntRange;

public interface PageLayout extends Iterable<RectFPlus>, Iterator<RectFPlus> {

    void initialize();

    RectFPlus getPositionAndSize(int page);

    int getPageCount();

    boolean isLoaded();

    IntRange getPagesInRect(RectFPlus area);

    RectFPlus getBoundingRect();

    /**
     * Add a callback that will be called, multiple callbacks allowed
     */
    void addLoadedCallback(PageLayoutLoadCallback callback);

    interface PageLayoutLoadCallback {
        void onLoaded();
    }
}
