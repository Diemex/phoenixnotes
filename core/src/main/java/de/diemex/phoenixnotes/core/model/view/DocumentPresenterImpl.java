package de.diemex.phoenixnotes.core.model.view;

import java.util.LinkedHashSet;
import java.util.Set;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.document.DocumentLoader;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;

/**
 * @param <T> class that will be used for in memory bitmaps. In Android this is Bitmap and in javafx Image
 */
public class DocumentPresenterImpl<T> implements DocumentPresenter<T>, OnPageLoadedListener<T> {
    Document       document;
    Viewport       viewport;
    RectFPlus      mScreenSize;
    DocumentLoader documentLoader;
    DocumentView view;
    PageCache    pageCache;
    boolean      animating;

    Set<OnPositionChangedListener>  mPositionListeners = new LinkedHashSet<>();
    Set<Animator>                   mAnimators         = new LinkedHashSet<>();
    Set<OnPageRangeChangedListener> mPageListeners     = new LinkedHashSet<>();


    public DocumentPresenterImpl(final DocumentView view, RectFPlus mScreenSize, Viewport viewport, Document document, DocumentLoader documentLoader, PageCache<T> pageCache) {
        this.document = document;
        this.viewport = viewport;
        this.mScreenSize = mScreenSize;
        this.documentLoader = documentLoader;
        this.view = view;
        this.pageCache = pageCache;

        registerPageListener(pageCache);
        registerPositionListener(viewport);

        view.setPresenter(this);
        viewport.addPageRangeListener(this);
        pageCache.setOnPageLoadedListener(this);

        if (!document.isLoaded())
            documentLoader.execute(new Document.DocumentLoadCallback() {
                @Override public void onError() {

                }


                @Override public void onLoaded() {
                    view.redraw();
                }
            });
    }


    @Override public void scroll(float dx, float dy) {
        if (isLoading()) return;
        for (OnPositionChangedListener positionChangedListener : mPositionListeners)
            positionChangedListener.scroll(dx, dy);
        view.redraw();
    }


    @Override public void zoom(float factor) {
        if (isLoading()) return;
        for (OnPositionChangedListener positionChangedListener : mPositionListeners)
            positionChangedListener.zoom(factor);
        view.redraw();
    }


    @Override public void computeNextFrame() {
        if (isLoading()) return;
        boolean stillAnimating = false;
        for (Animator animator : mAnimators) {
            if (animator.isAnimating()) stillAnimating = true;
            animator.computeAnimation();
        }
        if (!stillAnimating)
            stopAnimating();
    }


    @Override public void startAnimating() {
        if (isLoading()) return;
        animating = true;
        view.startAnimation();
    }


    @Override public void stopAnimating() {
        animating = false;
    }


    @Override public boolean isAnimating() {
        return animating;
    }


    private boolean isLoading() {
        return !document.isLoaded();
    }


    @Override public void loadPage(int page) {
        for (OnPageRangeChangedListener pageListener : mPageListeners)
            pageListener.loadPage(page);
    }


    @Override public void unloadPage(int page) {
        for (OnPageRangeChangedListener pageListener : mPageListeners)
            pageListener.unloadPage(page);
    }


    @Override public PageLayout getPageLayout() {
        return viewport.getPageLayout();
    }


    @Override public Viewport getViewPort() {
        return viewport;
    }


    @Override public PageCache<T> getPageRepo() {
        return pageCache;
    }


    @Override public void registerAnimator(Animator animator) {
        this.mAnimators.add(animator);
    }


    @Override public void registerPageListener(OnPageRangeChangedListener listener) {
        this.mPageListeners.add(listener);
    }


    @Override public void registerPositionListener(OnPositionChangedListener listener) {
        this.mPositionListeners.add(listener);
    }


    @Override public void onPageLoaded(T bitmap, int page, float zoom) {
        view.redraw();
    }
}
