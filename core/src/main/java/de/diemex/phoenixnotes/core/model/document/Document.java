package de.diemex.phoenixnotes.core.model.document;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;

public interface Document extends DocumentOrFolder {

    RectFPlus getPageSize(int page);

    int getPageCount();

    void load();

    /**
     * Has to have loaded: document name, page count and page sizes
     *
     * @return if document is loaded
     */
    boolean isLoaded();

    void addLoadedListener(DocumentLoadCallback callback);

    interface DocumentLoadCallback {
        void onError();

        void onLoaded();
    }
}
