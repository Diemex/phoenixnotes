package de.diemex.phoenixnotes.core.model.document;

import org.apache.commons.io.FileUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * I'm a XmlDocumentParser.
 * I parse and write information about a Document to a xml file.
 */
public class XmlDocumentParser extends DefaultHandler implements DocumentParser {
    private final DocumentImpl mDocument;
    private final File         mFile;
    private       String       mCurrentNodeValue;

    private static final String NOTEBOOK_NAME = "notebook_name";
    private static final String PAGE_COUNT    = "page_count";
    private static final String CURRENT_PAGE  = "current_page";
    private static final String X_OFFSET      = "x_offset";
    private static final String Y_OFFSET      = "y_offset";
    private static final String ZOOM          = "zoom";


    public XmlDocumentParser(DocumentImpl document, File file) {
        mDocument = document;
        mFile = file;
    }


    @Override public void endElement(String uri, String localName, String qName) throws SAXException {
        if (mCurrentNodeValue != null) {
            switch (qName) {
                case NOTEBOOK_NAME: {
                    mDocument.setName(mCurrentNodeValue);
                    break;
                }
                case PAGE_COUNT: {
                    int parsedVal = Integer.parseInt(mCurrentNodeValue);
                    mDocument.setPageCount(parsedVal);
                    break;
                }
                case CURRENT_PAGE: {
                    int parsedVal = Integer.parseInt(mCurrentNodeValue);
                    mDocument.setCurrentPage(parsedVal);
                    break;
                }
                case X_OFFSET: {
                    int parsedVal = Integer.parseInt(mCurrentNodeValue);
                    mDocument.setXOffset(parsedVal);
                    break;
                }
                case Y_OFFSET: {
                    int parsedVal = Integer.parseInt(mCurrentNodeValue);
                    mDocument.setYOffset(parsedVal);
                    break;
                }
                case ZOOM: {
                    float parsedVal = Float.parseFloat(mCurrentNodeValue);
                    mDocument.setZoom(parsedVal);
                    break;
                }
            }
        }
    }


    @Override public void characters(char[] ch, int start, int length) throws SAXException {
        mCurrentNodeValue = new String(ch, start, length);
    }


    @Override public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        mCurrentNodeValue = null;
    }


    @Override public void read() {
        try {
            SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
            parser.parse(mFile, this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }


    @Override public void write() {
        try {
            OutputStream outStream = FileUtils.openOutputStream(mFile);
            PrintWriter writer = new PrintWriter(outStream);
            //Super simple method for writing simple xml. It works
            writer.println("<notebook>");
            writer.println("<" + NOTEBOOK_NAME + ">" + mDocument.getName() + "</" + NOTEBOOK_NAME + ">");
            writer.println("<" + PAGE_COUNT + ">" + mDocument.getPageCount() + "</" + PAGE_COUNT + ">");
            writer.println("<" + CURRENT_PAGE + ">" + mDocument.getCurrentPage() + "</" + CURRENT_PAGE + ">");
            writer.println("<" + X_OFFSET + ">" + mDocument.getXOffset() + "</" + X_OFFSET + ">");
            writer.println("<" + Y_OFFSET + ">" + mDocument.getYOffset() + "</" + Y_OFFSET + ">");
            writer.println("<" + ZOOM + ">" + mDocument.getZoom() + "</" + ZOOM + ">");
            writer.println("</notebook>");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
