package de.diemex.phoenixnotes.core.model.model;

public class PagePosition {
    private float mX, mY;
    private int mPage;


    public PagePosition() {
        mX = mY = mPage = 0;
    }


    public PagePosition(int page, float x, float y) {
        this.mX = x;
        this.mY = y;
        this.mPage = page;
    }


    public float getX() {
        return mX;
    }


    public void setX(float mX) {
        this.mX = mX;
    }


    public float getY() {
        return mY;
    }


    public void setY(float mY) {
        this.mY = mY;
    }


    public int getPageNumber() {
        return mPage;
    }


    public void setPageNumber(int mPage) {
        this.mPage = mPage;
    }


    @Override public String toString() {
        return "page " + mPage + ": (" + mX + "|" + mY + ")";
    }


    @Override public boolean equals(Object obj) {
        if (!(obj instanceof PagePosition)) return false;
        PagePosition pos = (PagePosition) obj;
        return mPage == pos.mPage && mX == pos.mX && mY == pos.mY;
    }
}
