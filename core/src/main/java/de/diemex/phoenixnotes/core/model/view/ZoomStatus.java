package de.diemex.phoenixnotes.core.model.view;

public enum ZoomStatus {
    FIT_WIDTH,
    ZOOMED_IN,
    ZOOMED_OUT
}
