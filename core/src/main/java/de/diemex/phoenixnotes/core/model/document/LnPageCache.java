package de.diemex.phoenixnotes.core.model.document;

import java.util.HashMap;
import java.util.Map;

import de.diemex.phoenixnotes.core.model.view.OnPageLoadedListener;
import de.diemex.phoenixnotes.core.model.view.PageCache;


public class LnPageCache<T> implements PageCache<T>, OnPageLoadedListener<T> {
    private LnDocument      mDocument;
    private LnPageLoader<T> mPageLoader;
    private Map<Integer, T> mPages = new HashMap<>();
    private OnPageLoadedListener<T> listener;


    public LnPageCache(LnDocument document, LnPageLoader<T> pageLoader) {
        this.mDocument = document;
        this.mPageLoader = pageLoader;
    }


    @Override public void loadPage(int page) {
        mPageLoader.loadPage(page, 1, this);
    }


    @Override public T getPage(int page) {
        return mPages.get(page);
    }


    @Override public boolean isPageLoaded(int page) {
        return mPages.containsKey(page);
    }


    @Override public void unloadPage(int page) {
        mPages.remove(page);
    }


    @Override public void onPageLoaded(T bitmap, int page, float zoom) {
        mPages.put(page, bitmap);
        if (listener != null)
            listener.onPageLoaded(bitmap, page, zoom);
    }


    @Override public void setOnPageLoadedListener(OnPageLoadedListener<T> listener) {
        this.listener = listener;
    }
}

