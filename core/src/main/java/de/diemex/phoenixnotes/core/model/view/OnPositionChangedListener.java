package de.diemex.phoenixnotes.core.model.view;

public interface OnPositionChangedListener {
    void scroll(float dx, float dy);

    void zoom(float factor);
}
