package de.diemex.phoenixnotes.core.model.executor;

import org.apache.commons.lang3.Validate;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadExecutor implements InteractorExecutor {
    private final static int                     POOL_SIZE       = 2;
    private final static int                     MAX_POOL_SIZE   = 4;
    private final static int                     KEEP_ALIVE_TIME = 120;
    private final static TimeUnit                TIME_UNIT       = TimeUnit.SECONDS;
    private final static BlockingQueue<Runnable> WORK_QUEUE      = new LinkedBlockingQueue<>();
    private ThreadPoolExecutor mExecutor;


   public ThreadExecutor() {
        mExecutor = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TIME_UNIT, WORK_QUEUE);
    }


    @Override public void run(final Interactor interactor) {
        Validate.notNull(interactor);
        mExecutor.submit(new Runnable() {
            @Override public void run() {
                interactor.run();
            }
        });
    }
}
