package de.diemex.phoenixnotes.core.model.view;

public interface DocumentView {
    void redraw();

    void startAnimation();

    void setPresenter(DocumentPresenter presenter);

    DocumentPresenter getPresenter();
}
