package de.diemex.phoenixnotes.core.model.model;

public class SimplePointF implements PointF {
    float x, y;


    public SimplePointF() {
        x = y = 0;
    }


    public SimplePointF(float x, float y) {
        this.x = x;
        this.y = y;
    }


    @Override public float getX() {
        return x;
    }


    @Override public float getY() {
        return y;
    }


    @Override public void setX(float x) {
        this.x = x;
    }


    @Override public void setY(float y) {
        this.y = y;
    }
}
