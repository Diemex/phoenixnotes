package de.diemex.phoenixnotes.core.model.view;

public interface OnPageLoadedListener<T> {
    void onPageLoaded(T bitmap, int page, float zoom);
}
