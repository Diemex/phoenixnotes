package de.diemex.phoenixnotes.core.model.document;

import org.apache.commons.io.FileUtils;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.model.SimpleRectF;
import de.diemex.phoenixnotes.core.model.util.IntRange;
import de.diemex.phoenixnotes.core.model.util.ParseMethods;

public class LnDocument implements Document {
    private boolean mLoaded;
    private int mPageCount;
    private int mLayerCount = 1;
    private File               mFolder;
    private InteractorExecutor mExecutor;
    private RectFPlus                  mPageSize = new SimpleRectF();
    private List<DocumentLoadCallback> callbacks = new ArrayList<>();

    private final static String mSettingsFileName = "notebook.xml";


    public LnDocument(File mFolder) {
        this.mFolder = mFolder;
    }


    public LnDocument(String path) {
        this.mFolder = new File(path);
    }


    public void load() {
        loadPageCount();
        loadNotebookInfo();
        mLoaded = true;
        for (DocumentLoadCallback callback : callbacks)
            callback.onLoaded();
    }


    @Override public void addLoadedListener(DocumentLoadCallback callback) {
        callbacks.add(callback);
    }


    @Override public boolean isLoaded() {
        return mLoaded;
    }


    public void setPagesize(RectFPlus size) {
        mPageSize = size;
    }


    @Override public RectFPlus getPageSize(int page) {
        return mPageSize;
    }


    @Override public int getPageCount() {
        return mPageCount;
    }


    public void setLayerCount(int layerCount) {
        mLayerCount = layerCount;
    }


    public int getLayerCount() {
        return mLayerCount;
    }


    @Override public String getName() {
        return mFolder.getName();
    }


    public void saveNotebookInfoToFile() {
        try {
            OutputStream outStream = FileUtils.openOutputStream(new File(mFolder, mSettingsFileName));
            PrintWriter writer = new PrintWriter(outStream);
            //Super simple method for writing simple xml
            writer.println("<notebook>");
            writer.println("<paperwidth>" + mPageSize.width() + "</paperwidth>");
            writer.println("<paperheight>" + mPageSize.height() + "</paperheight>");
            writer.println("<layers>" + mLayerCount + "</layers>");
            writer.println("</notebook>");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void loadPageCount() {
        //"page.png" is required
        File firstPage = new File(mFolder, "page1.png");
        if (!firstPage.exists() || firstPage.isDirectory()) {
            mPageCount = 0;
            return;
        }

        final Pattern pagePat = Pattern.compile("page[0-9]+.png");
        List<String> fileNames = Arrays.asList(mFolder.list(new FilenameFilter() {
            @Override public boolean accept(File dir, String filename) {
                return pagePat.matcher(filename).matches();
            }
        }));

        boolean[] pages = new boolean[fileNames.size() + 2];
        for (String fileName : fileNames) {
            int parsed = Integer.parseInt(fileName.substring(4, fileName.length() - 4)); //Cut between page and .png
            pages[parsed] = true;
        }
        //No pages are allowed to be missing
        int highest = 1;
        for (int page : new IntRange(2, pages.length - 1))
            if (pages[page]) highest = page;
            else break;
        mPageCount = highest;
    }


    public File getBitmapFile(int page, int layer) {
        if (layer == 1) return new File(mFolder, "page" + (page + 1) + ".png");
        else return new File(mFolder, "page" + (page + 1) + "_" + layer + ".png");
    }


    private void loadNotebookInfo()
    {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = new File(mFolder.getPath(), mSettingsFileName);
        if (!xmlFile.exists() || xmlFile.isDirectory()) return;
        try {
            org.jdom.Document document = builder.build(xmlFile);
            Element rootNode = document.getRootElement();

            for (Object child : rootNode.getChildren()) {
                Element node = (Element) child;
                if (node.getName().equals("paperwidth")) {
                    if (!ParseMethods.isValidFloat(node.getValue()))
                        System.out.println("Invalid Page Width");
                    else {
                        float width = Float.parseFloat(node.getValue());
                        mPageSize.setRight(width);
                    }
                } else if (node.getName().equals("paperheight")) {
                    if (!ParseMethods.isValidFloat(node.getValue()))
                        System.out.println("Invalid Page Height");
                    else {
                        float height = Float.parseFloat(node.getValue());
                        mPageSize.setBottom(height);
                    }
                } else if (node.getName().equals("layers")) {
                    if (!ParseMethods.isValidInt(node.getValue()))
                        System.out.println("Invalid Layer Count");
                    else
                        mLayerCount = Integer.parseInt(node.getValue());
                }
            }
        } catch (IOException | JDOMException e) {
            System.out.println(e.getMessage());
        }
    }


    @Override public String toString() {
        return getName();
    }
}
