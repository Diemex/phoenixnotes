package de.diemex.phoenixnotes.core.model.util;

import java.util.regex.Pattern;

public class ParseMethods {
    //Optional minus sign, optional dot, only matches digits and needs at least one (+ sign), matches floats and integers
    private static Pattern mFloatPattern = Pattern.compile("[-]?\\d*\\.?\\d+");

    //Matches one digit or more, doesn't match empty strings
    private static Pattern mIntPattern = Pattern.compile("\\d+");


    public static boolean isValidFloat(String input) {
        return mFloatPattern.matcher(input).matches();
    }


    public static boolean isValidInt(String input) {
        return mIntPattern.matcher(input).matches();
    }
}
