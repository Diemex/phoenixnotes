package de.diemex.phoenixnotes.core.model.document;

public interface DocumentOrFolder {
    String getName();
}
