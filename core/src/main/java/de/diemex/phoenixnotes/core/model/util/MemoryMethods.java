package de.diemex.phoenixnotes.core.model.util;

public class MemoryMethods {
    public static long getAvailableBytes() {
        return Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory() + Runtime.getRuntime().freeMemory();
    }


    public static int getAvailableKilos() {
        return (int) (getAvailableBytes() / 1024);
    }


    public static int getAvailableMegs() {
        return (int) (getAvailableBytes() / 1048576L);
    }
}
