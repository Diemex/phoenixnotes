package de.diemex.phoenixnotes.core.model.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.diemex.phoenixnotes.core.model.document.Document;
import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.model.SimpleRectF;
import de.diemex.phoenixnotes.core.model.util.IntRange;

/**
 * I'm a VerticalViewportThing.
 * I calculate the positions of all pages, I put them below each other.
 * I provide a WorldCoordinateSystem.
 */
public class VerticalPageLayout implements PageLayout {
    private Document mDocument; //singleton
    private Iterator<RectFPlus> iter; //From List
    private List<RectFPlus>              mPagePositions = new ArrayList<>();
    private RectFPlus                    mBoundingRect  = new SimpleRectF();
    private boolean                      isLoaded       = false;
    private List<PageLayoutLoadCallback> callbacks      = new ArrayList<>();


    public VerticalPageLayout(Document document) {
        this.mDocument = document;
        if (document.isLoaded()) {
            initialize();
            isLoaded = true;
        } else
            document.addLoadedListener(new Document.DocumentLoadCallback() {
                @Override public void onError() {

                }


                @Override public void onLoaded() {
                    initialize();
                }
            });
    }


    @Override public void initialize() {
        mPagePositions.clear();
        float offsetY = 0;
        for (int page = 0; page < mDocument.getPageCount(); page++) {
            RectFPlus pageSize = mDocument.getPageSize(page);
            mPagePositions.add(new SimpleRectF(0, offsetY, pageSize.width(), pageSize.height() + offsetY));
            offsetY += pageSize.height();
        }
        mBoundingRect.set(0, 0, mPagePositions.get(mDocument.getPageCount() - 1).right(), mPagePositions.get(mDocument.getPageCount() - 1).bottom());
        for (PageLayoutLoadCallback callback : callbacks)
            callback.onLoaded();
    }


    @Override public RectFPlus getPositionAndSize(int page) {
        return mPagePositions.get(page);
    }


    @Override public int getPageCount() {
        return mPagePositions.size();
    }


    @Override public IntRange getPagesInRect(RectFPlus area) {
        IntRange pageRange = new IntRange(-1, -1);
        int startingPage = 0;
        //find starting page
        for (RectFPlus pagePos : this) {
            if (pagePos.bottom() > area.top()) {
                pageRange.setStart(startingPage);
                break;
            }
            startingPage++;
        }
        int endPage = 0;
        //find end page
        for (RectFPlus pagePos : this) {
            if (pagePos.bottom() >= area.bottom()) {
                pageRange.setStop(endPage);
                break;
            }
            endPage++;
        }
        return pageRange;
    }


    @Override public RectFPlus getBoundingRect() {
        return mBoundingRect;
    }
    //Iterator Stuff


    @Override public Iterator<RectFPlus> iterator() {
        iter = mPagePositions.iterator();
        return iter;
    }


    @Override public boolean hasNext() {
        return iter.hasNext();
    }


    @Override public RectFPlus next() {
        return iter.next();
    }


    @Override public void remove() {
        throw new UnsupportedOperationException();
    }


    @Override public boolean isLoaded() {
        return isLoaded;
    }


    @Override public void addLoadedCallback(PageLayoutLoadCallback callback) {
        callbacks.add(callback);
    }
}