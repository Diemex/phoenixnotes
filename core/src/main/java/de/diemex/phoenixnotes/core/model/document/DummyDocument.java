package de.diemex.phoenixnotes.core.model.document;

import java.util.ArrayList;
import java.util.List;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.model.SimpleRectF;
import de.diemex.phoenixnotes.core.model.util.IntRange;

public class DummyDocument implements Document {

    private List<RectFPlus> mPageSizes = new ArrayList<>();


    public DummyDocument(float width, float height, int pages) {
        for (int page : new IntRange(pages))
            mPageSizes.add(new SimpleRectF(width, height));
    }


    public DummyDocument(float width, float height) {
        this(width, height, 100);
    }


    public DummyDocument() {
        this(210, 300);
    }


    /**
     * @param sizes {{width1, height1, pages1}, {width2, height2, pages2}}
     */
    public DummyDocument(int[][] sizes) {
        for (int[] size : sizes)
            for (int u = 0; u < size[2]; u++)
                mPageSizes.add(new SimpleRectF(size[0], size[1]));
    }


    @Override public RectFPlus getPageSize(int page) {
        return mPageSizes.get(page);
    }


    @Override public int getPageCount() {
        return mPageSizes.size();
    }


    @Override public void load() {

    }


    @Override public boolean isLoaded() {
        return true;
    }


    @Override public String getName() {
        return null;
    }


    @Override public void addLoadedListener(DocumentLoadCallback callback) {

    }
}
