package de.diemex.phoenixnotes.core.model.document;

import java.io.File;

public class FolderImpl implements Folder {
    private final File mFolder;


    public FolderImpl(File folder) {
        this.mFolder = folder;
    }


    @Override public String getName() {
        return mFolder.getName();
    }


    @Override public String toString() {
        return getName();
    }
}
