package de.diemex.phoenixnotes.core.model.view;

import de.diemex.phoenixnotes.core.model.model.RectFPlus;
import de.diemex.phoenixnotes.core.model.util.IntRange;

public interface Viewport extends OnPositionChangedListener {
    void addPageRangeListener(OnPageRangeChangedListener listener);

//    void initialize();

    void layout();

//    boolean isLoaded();

//    PointF getPagePosition(int page);

//    RectFPlus getPageSize(int page);

    PageLayout getPageLayout();

    IntRange getDisplayedPageRange();

    int getDisplayedPageCount();

    RectFPlus getViewportSize();

//    float getOffsetX();

//    float getOffsetY();

    float getZoomFactor();

    void scroll(float dx, float dy);

    /**
     * Scroll a number of pages can be fractions for pagelayouts that support it.
     *
     * @param pages pages to scroll fractions supported
     */
    void scrollPages(float pages);

    void zoom(float factor);
}
