package de.diemex.phoenixnotes.core.model.model;

public interface RectFPlus {
    float top();

    float left();

    float bottom();

    float right();

    RectFPlus setTop(float top);

    RectFPlus setLeft(float left);

    RectFPlus setBottom(float bottom);

    RectFPlus setRight(float right);

    RectFPlus set(float left, float top, float right, float bottom);

    float width();

    float height();

    void offset(float dx, float dy);
}
