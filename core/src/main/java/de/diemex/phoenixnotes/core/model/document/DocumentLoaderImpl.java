package de.diemex.phoenixnotes.core.model.document;

import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.executor.MainThread;

public class DocumentLoaderImpl implements DocumentLoader {
    private Document              document;
    private MainThread            mainThread;
    private InteractorExecutor    executor;
    private Document.DocumentLoadCallback callback;


    public DocumentLoaderImpl(Document document, MainThread mainThread, InteractorExecutor interactorExecutor) {
        this.document = document;
        this.mainThread = mainThread;
        this.executor = interactorExecutor;
    }


    @Override public void execute(Document.DocumentLoadCallback callback) {
        this.callback = callback;
        this.executor.run(this);
    }


    @Override public void run() {
        document.load();
        if (callback != null)
        mainThread.post(new Runnable() {
            @Override public void run() {
                callback.onLoaded();
            }
        });
    }
}
