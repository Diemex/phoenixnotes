package de.diemex.phoenixnotes.core.model.model;

public interface PointF {
    float getX();

    float getY();

    void setX(float x);

    void setY(float y);
}
