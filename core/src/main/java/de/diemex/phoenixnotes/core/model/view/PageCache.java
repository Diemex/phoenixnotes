package de.diemex.phoenixnotes.core.model.view;

public interface PageCache<T> extends OnPageRangeChangedListener {
    T getPage(int page);

    boolean isPageLoaded(int page);

    void setOnPageLoadedListener(OnPageLoadedListener<T> listener);
}
