package de.diemex.phoenixnotes.core.model.model;

public class SimpleRectF implements RectFPlus {
    float top, left, bottom, right;


    public SimpleRectF(float left, float top, float right, float bottom) {
        this.top = top;
        this.left = left;
        this.bottom = bottom;
        this.right = right;
    }


    public SimpleRectF(float width, float height) {
        top = left = 0;
        right = width;
        bottom = height;
    }


    public SimpleRectF(RectFPlus copy) {
        this.left = copy.left();
        this.top = copy.top();
        this.right = copy.right();
        this.bottom = copy.bottom();
    }


    public SimpleRectF() {
    }


    @Override public float top() {
        return top;
    }


    @Override public float left() {
        return left;
    }


    @Override public float bottom() {
        return bottom;
    }


    @Override public float right() {
        return right;
    }


    @Override public RectFPlus setTop(float top) {
        this.top = top;
        return this;
    }


    @Override public RectFPlus setLeft(float left) {
        this.left = left;
        return this;
    }


    @Override public RectFPlus setBottom(float bottom) {
        this.bottom = bottom;
        return this;
    }


    @Override public RectFPlus setRight(float right) {
        this.right = right;
        return this;
    }


    @Override public RectFPlus set(float left, float top, float right, float bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        return this;
    }


    @Override public float width() {
        return right - left;
    }


    @Override public float height() {
        return bottom - top;
    }


    @Override public void offset(float dx, float dy) {
        top += dy;
        bottom += dy;
        left += dx;
        right += dx;
    }


    @Override public boolean equals(Object o) {
        if (!(o instanceof RectFPlus)) return false;
        RectFPlus rect = (RectFPlus) o;
        return left == rect.left() && top == rect.top() && right == rect.right() && bottom == rect.bottom();
    }


    @Override public String toString() {
        return "SimpleRectF(" + left + ", " + top + ", " + right + ", " + bottom + ", w: " + width() + ", h: " + height() + ")";
    }
}
