package de.diemex.phoenixnotes.core.model.view;

public interface DocumentPresenter<T> extends OnPositionChangedListener, OnPageRangeChangedListener {
    //Called by the View. The Android System will choose the framerate.
    void computeNextFrame();

    void startAnimating();

    void stopAnimating();

    boolean isAnimating();

    Viewport getViewPort();

    PageLayout getPageLayout();

    PageCache<T> getPageRepo();

    void registerAnimator(Animator animator);

    void registerPageListener(OnPageRangeChangedListener listener);

    void registerPositionListener(OnPositionChangedListener listener);

    //OnPositionChanged

    void scroll(float dx, float dy);

    void zoom(float factor);

    //OnPageRangeChanged

    void unloadPage(int page);

    void loadPage(int page);
}
