package de.diemex.phoenixnotes.core.model.model;


import java.util.ArrayList;
import java.util.List;

import de.diemex.phoenixnotes.core.model.document.DocumentOrFolder;

public class Node {
    private List<Node> children = new ArrayList<>();
    private Node   parent;
    private DocumentOrFolder value;


    public Node() {

    }


    public Node(DocumentOrFolder value) {
        this.value = value;
    }


    public Node(Node parent, DocumentOrFolder value) {
        this.parent = parent;
        this.value = value;
    }


    public void addChild(Node child) {
        children.add(child);
    }


    public void setValue(DocumentOrFolder value) {
        this.value = value;
    }


    public List<DocumentOrFolder> getValueList() {
        List<DocumentOrFolder> values = new ArrayList<>();
        for (Node node : children)
            values.add(node.getValue());
        return values;
    }


    public List<Node> getChildren() {
        return children;
    }


    public DocumentOrFolder getValue() {
        return value;
    }


    public boolean isLeaf() {
        return children.size() == 0;
    }


    /**
     * Get all nodes that have children
     */
    public List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();
        for (Node node : getChildren())
            if (!node.isLeaf())
                nodes.add(node);
        return nodes;
    }
}
