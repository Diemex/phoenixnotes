package de.diemex.phoenixnotes.core.model.executor;


public interface MainThread {
    void post(final Runnable runnable);
}
