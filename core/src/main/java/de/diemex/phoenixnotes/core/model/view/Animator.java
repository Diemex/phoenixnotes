package de.diemex.phoenixnotes.core.model.view;

public interface Animator {
    boolean isAnimating();

    boolean computeAnimation();
}
