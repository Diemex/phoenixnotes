package de.diemex.phoenixnotes.core.model.document;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import de.diemex.phoenixnotes.core.model.model.Node;

public class LnDocumentProvider {
    private File mRootFile;
    private Node mRootNode = new Node(null);
    private List<LnDocument> mLnDocuments = new ArrayList<>();


    public LnDocumentProvider(File rootFile) {
        this.mRootFile = rootFile;
    }


    public void load() {
        traverse(mRootFile, mRootNode);
    }


    public void traverse(File parent, Node parentNode) {
        if (parent.exists()) {
            File[] files = parent.listFiles(new FileFilter() {
                @Override public boolean accept(File pathname) {
                    return pathname.isDirectory() || pathname.getName().equals("notebook.xml");
                }
            });
            for (File file : files) {
                if (file.isDirectory()) {
                    Node node = new Node(new FolderImpl(file));
                    parentNode.addChild(node);
                    traverse(file, node);
                } else {
                    //File is /documentName/notebook.xml -> pass in /documentName/ instead
                    //So after we traversed into a folder we realize it's a notebook and we change it
                    LnDocument doc = new LnDocument(file.getParentFile());
                    mLnDocuments.add(doc);
                    parentNode.setValue(doc);
                }
            }
        }
    }


    public List<LnDocument> getDocuments() {
        return mLnDocuments;
    }


    public Node getRootNode() {
        return mRootNode;
    }
}
