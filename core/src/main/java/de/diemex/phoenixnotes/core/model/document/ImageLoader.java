package de.diemex.phoenixnotes.core.model.document;

import java.io.File;

public interface ImageLoader<T> {
    T load(File path);
}
