package de.diemex.phoenixnotes.core.model.executor;

public interface Interactor {
    void run();
}
