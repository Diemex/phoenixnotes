package de.diemex.phoenixnotes.core.model.document;

import de.diemex.phoenixnotes.core.model.executor.Interactor;
import de.diemex.phoenixnotes.core.model.executor.InteractorExecutor;
import de.diemex.phoenixnotes.core.model.view.OnPageLoadedListener;

public class LnPageLoader<T> {
    private LnDocument         mDocument;
    private InteractorExecutor mExecutor;
    private ImageLoader<T>     mLoader;


    public LnPageLoader(InteractorExecutor executor, LnDocument mDocument, ImageLoader<T> loader) {
        this.mDocument = mDocument;
        this.mExecutor = executor;
        this.mLoader = loader;
    }


    public void loadPage(final int page, final float zoom, final OnPageLoadedListener<T> callback) {
        mExecutor.run(new Interactor() {
            @Override public void run() {
                T bmp = mLoader.load(mDocument.getBitmapFile(page, 1));
                callback.onPageLoaded(bmp, page, zoom);
            }
        });
    }
}
