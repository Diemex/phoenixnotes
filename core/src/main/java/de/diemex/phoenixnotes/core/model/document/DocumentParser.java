package de.diemex.phoenixnotes.core.model.document;

public interface DocumentParser {

    void read();

    void write();
}
